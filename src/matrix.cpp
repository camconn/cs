/*
 * matrix.cpp - Matrix Utilities
 *
 * © 2020 Cameron Conn
 */


#include <cairo.h>

#include "graphics.hpp"
#include "matrix.hpp"


// Get a copy of the matrix that transforms Cairo user-space to
// PostScript coordinates.
cairo_matrix_t cairo_to_ps_matrix() {
  // Cairo's origin is the top-left of the image. Postscript's is the bottom-left
  // of the image. This is a cute temporary hack that translates stuff
  // so that Cairo is using a Postscript coordinate space.
  cairo_matrix_t cairo_to_ps;
  cairo_matrix_init_identity(&cairo_to_ps);
  reflect_y(&cairo_to_ps);
  cairo_to_ps.y0 = pg_height; // translate up by page height
  return cairo_to_ps;
}

void reflect_y(cairo_matrix_t* mat) {
  mat->yy *= -1;
}
