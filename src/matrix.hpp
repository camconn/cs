/*
 * matrix.hpp - Matrix Utilities
 *
 * © 2020 Cameron Conn
 */

#ifndef CS_MATRIX_H
#define CS_MATRIX_H

#include <cairo.h>

cairo_matrix_t cairo_to_ps_matrix();

void reflect_y(cairo_matrix_t*);


#endif

