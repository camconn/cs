/*
 * util.hpp - Common Utilities
 *
 * © 2020 Cameron Conn
 */

#ifndef CS_UTIL_H
#define CS_UTIL_H

bool str_equals(const char* a, const char* b);


#endif
