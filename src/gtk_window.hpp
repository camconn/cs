/*
 * gtk_window.hpp - GTK UI Setup
 *
 * © 2020 Cameron Conn
 */

#ifndef CS_GTK_WINDOW_H
#define CS_GTK_WINDOW_H

#include <cairo.h>

int create_window(cairo_surface_t*);


#endif
