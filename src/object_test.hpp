/*
 * object_test.hpp - Convenience functions for Google Test
 *
 * © 2020 Cameron Conn
 */

#ifndef CS_OBJECT_TEST_H
#define CS_OBJECT_TEST_H

#include <iosfwd>

#include "object.hpp"

std::ostream& operator<< (std::ostream&, const Object&);
std::ostream& operator<< (std::ostream& os, Object*);


#endif
