/*
 * op.hpp - Native PostScript operators.
 *
 * © 2020 Cameron Conn
 */

#ifndef CS_OP_H
#define CS_OP_H

#include "interpreter.hpp"
#include "memory.hpp"

// Convenience macros for defining native operators so that we
// don't manually need to change the function prototype later on
// in every single place that it is used.
//
// This macro defines an operator used by the PostScript interpreter
// in the form of:
//   void native_name(Stack stack).
#define NATIVE(name) void native_ ## name(Stack stack)

// Macro used for requiring a number of arguments within a
// compiled PostScript operator. It isn't used in this file, but
// is used in almost all operator implementations.
#define REQUIRE_ARGS(stack, num)                    \
if (stack->length < num) {                          \
 g_queue_push_tail(stack,                           \
                   allocator->create_exception());  \
  return;                                           \
}

// op.cpp
NATIVE(clear);
NATIVE(cleartomark);
NATIVE(copy);
NATIVE(count);
NATIVE(counttomark);
NATIVE(dup);
NATIVE(exch);
NATIVE(index);
NATIVE(mark);
NATIVE(pop);
NATIVE(print);
NATIVE(print_stack);
NATIVE(roll);

// op_bool.cpp
NATIVE(eq);
NATIVE(ne);
NATIVE(gt);
NATIVE(ge);
NATIVE(lt);
NATIVE(le);
NATIVE(and);
NATIVE(or);
NATIVE(xor);

// op_dict.cpp
NATIVE(begin);
NATIVE(def);
NATIVE(dict);
NATIVE(end);

// op_font.cpp
NATIVE(findfont);
NATIVE(scalefont);
NATIVE(setfont);

// op_math.cpp
NATIVE(add);
NATIVE(sub);
NATIVE(mul);
NATIVE(div);
NATIVE(idiv);
NATIVE(mod);
NATIVE(abs);
NATIVE(neg);
NATIVE(ceil);
NATIVE(floor);
NATIVE(round);
NATIVE(trunc);
NATIVE(sqrt);
NATIVE(exp);
NATIVE(ln);
NATIVE(log);
NATIVE(sin);
NATIVE(cos);
NATIVE(atan);
NATIVE(rand);
NATIVE(srand);
NATIVE(rrand);

// op_graphics.cpp
NATIVE(newpath);
NATIVE(closepath);
NATIVE(moveto);
NATIVE(lineto);
NATIVE(rlineto);
NATIVE(rmoveto);
NATIVE(arc);
NATIVE(curveto);
NATIVE(setgray);
NATIVE(currentgray);
NATIVE(setlinewidth);
NATIVE(currentlinewidth);
NATIVE(fill);
NATIVE(stroke);
NATIVE(show);
NATIVE(showpage);

#endif
