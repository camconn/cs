/*
 * op_dict.cpp - PostScript dictionary operators.
 *
 * © 2020 Cameron Conn
 */

#include <cstdlib>

#include "common.hpp"
#include "memory.hpp"
#include "object.hpp"
#include "op.hpp"

NATIVE(begin) {
  REQUIRE_ARGS(stack, 1);
  auto dict = (Object*) g_queue_peek_tail(stack);
  if (dict->type != Dictionary) {
    g_queue_push_tail(stack, allocator->create_exception());
    return;
  }

  g_queue_pop_tail(stack);

  g_ptr_array_add(allocator->dict_stack, dict->dict);
}

// TODO: Implement permissions for writing dictionaries.
NATIVE(def) {
  REQUIRE_ARGS(stack, 2);
  auto val = (Object*) g_queue_pop_tail(stack);
  auto name = (Object*) g_queue_pop_tail(stack);

  if (name->type != Name && name->attribs[ExecutableBit] == 0) {
    g_queue_push_tail(stack, allocator->create_exception());
    return;
  }

  std::string name_str = *name->name;

  // Look up the key in the dictionary stack.
  // Note that we DON'T look in the system/global dictionary stack here.
  for (int i = allocator->dict_stack->len - 1;
       i >= 0;
       i--) {

    auto d = (PSDict*) g_ptr_array_index(allocator->dict_stack, i);

    Dict* di = d->dict;
    Dict::iterator found = di->find(name_str);
    if (found != di->end()) {
      (*di)[name_str] = val;
      return;
    }
  }

  // If we couldn't find the key earlier, just place it in the
  // dictionary on the top of the dictionary stack.
  //if (allocator->dict_stack.empty()) {
  if (allocator->dict_stack->len == 0) {
    fprintf(stderr, "dictionary stack is empty?!?!\n");
    g_queue_push_tail(stack, allocator->create_exception());
    return;
  }

  auto top = (PSDict*) g_ptr_array_index(allocator->dict_stack,
                                         allocator->dict_stack->len - 1);
  top->dict->insert(std::make_pair(name_str, val));
}

NATIVE(dict) {
  REQUIRE_ARGS(stack, 1);
  auto top = (Object*) g_queue_peek_tail(stack);
  if (top->type != Integer) {
    //typecheck
    g_queue_push_tail(stack, allocator->create_exception());
    return;
  }

  g_queue_pop_tail(stack);
  g_queue_push_tail(stack, allocator->create_dict());
}

NATIVE(end) {
  auto dictNum = allocator->dict_stack->len;
  if (dictNum - 1 <= USER_DICT) {
    // dictstackunderflow
    g_queue_push_tail(stack, allocator->create_exception());
    return;
  }

  g_ptr_array_set_size(allocator->dict_stack, dictNum - 1);
}
