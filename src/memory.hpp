/*
 * memory.hpp - Garbage Collector and Memory Allocators
 *
 * © 2020 Cameron Conn
 */

#ifndef CS_MEMORY_H
#define CS_MEMORY_H

#include <cstdint>

#include <map>
#include <string>

#include <glib.h>

#include "object.hpp"

using std::map;
using std::string;

typedef map<string,Object*> Dict;
typedef GQueue* Stack;
typedef void (*PSProcedure)(Stack);

constexpr size_t SYSTEM_DICT = 0;
constexpr size_t GLOBAL_DICT = 1;
constexpr size_t USER_DICT = 2;

/**
 * A VM holds weak references 
 */
struct VM {
  /** Maps reference IDs to objects */
  GPtrArray *objs;    // Object*
  GPtrArray *fonts;   // Object*

  GPtrArray *arrays;  // PSArray*
  GPtrArray *dicts;   // PSDict*
  GPtrArray *strings; // PSString*

  PSDict* builtin_dicts[3];
  GPtrArray *dict_stack; // PSDict*

  /** A manual call to the garbage collector. */
  //void collect_garbage(Stack a, Stack b);
  void collect_garbage(GQueue*, GQueue*);

  // PRNG Seed
  int64_t random_seed;

  // Garbage Collected Base Objects
  Object* create_gc_obj();
  PSArray* create_gc_array(GPtrArray*);
  PSDict* create_gc_dict(map<string,Object*>);
  PSString* create_gc_string(const string&);

  // Base Types
  Object* create_array(PSArray*);
  Object* create_boolean(bool value);
  Object* create_boolean(const char*);
  Object* create_dict();
  Object* create_dict(int64_t);
  Object* create_exception();
  Object* create_int(const char*);
  Object* create_int(int64_t);
  Object* create_font(int64_t);
  Object* create_font(const string&);
  Object* create_mark();
  Object* create_mark_end();
  Object* create_null();
  Object* create_procedure(PSProcedure);
  //check interpreter.cpp for create_procedure_native
  Object* create_real(const char*);
  Object* create_real(double);
  Object* create_string(const char*, size_t len);
  Object* create_string(const string&);
  Object* create_string(PSString*);

  // Derived types
  Object* create_name(const char*, size_t len);
  Object* create_name(const string&);
  Object* create_name_literal(const char*, size_t len);
  Object* create_name_literal(const string&);

  // Memory Management
  Object* clone_object(Object* obj);
  Object* clone_object(Object obj);

  // lookup something recursively in VM namespace.
  Object* lookup(const string&);

private:
  void gc_mark_internals();
  void gc_mark(GQueue*);
  void gc_reset();
  void gc_sweep();
};

extern VM* allocator;

/** Initialize PostScript "Virtual Machine" memory. */
void init_vm();

#endif

