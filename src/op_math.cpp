/*
 * op.cpp - Math-oriented PostScript operators.
 *
 * © 2020 Cameron Conn
 */

#include <cassert>
#include <cmath>
#include <cstdio>

#include "interpreter.hpp"
#include "object.hpp"
#include "op.hpp"

// Wrapper for binary math operations
#define BINARY_MATH_OP(name, op)              \
NATIVE(name) {                                \
  REQUIRE_ARGS(stack, 2);                     \
  auto b = (Object*) g_queue_pop_tail(stack); \
  auto a = (Object*) g_queue_pop_tail(stack); \
  Object* result = (*a) op (*b);              \
  g_queue_push_tail(stack, result);           \
}

// Used for ceil, truncate, et al.
#define INTEGER_IDENTITY_FUNC(name, func)           \
NATIVE(name) {                                      \
  REQUIRE_ARGS(stack, 1);                           \
  auto a = (Object*) g_queue_peek_tail(stack);      \
  if (is_int(a)) {                                  \
    /* do nothing, identity function */             \
  } else if (is_real(a)) {                          \
    g_queue_pop_tail(stack);                        \
    double val = a->d;                              \
    g_queue_push_tail(stack, allocator->create_real(func(val))); \
  } else {                                          \
    g_queue_pop_tail(stack);                        \
    g_queue_push_tail(stack, allocator->create_exception()); \
  }                                                 \
}

// Used for trigonometric functions.
#define RANGED_FUNC(name, func, r_low, r_high)  \
NATIVE(name) {                                  \
  REQUIRE_ARGS(stack, 1);                       \
  auto o = (Object*) g_queue_pop_tail(stack);   \
  if (is_num(o)) {                              \
    double value = get_double(o);               \
    if (value >= r_low && value <= r_high) {    \
      double res = func(value);                 \
      Object* result = allocator->create_real(res); \
      g_queue_push_tail(stack, result);         \
    } else {                                    \
      g_queue_push_tail(stack, allocator->create_exception()); \
    }                                           \
  } else {                                      \
    g_queue_push_tail(stack, allocator->create_exception()); \
  }                                             \
}

// Used to convert PostScript parameters of degrees
// into radians accepted by C math library.
#define DEG_TO_RAD_WRAPPER(name)                \
NATIVE(name) {                                  \
  REQUIRE_ARGS(stack, 1);                       \
  auto top = (Object*) g_queue_pop_tail(stack); \
  double deg, rad;                              \
  if (!is_num(top)) {                           \
    g_queue_push_tail(stack, allocator->create_exception()); \
    return;                                     \
  }                                             \
  rad = get_double(top) * M_PI / 180;           \
  g_queue_push_tail(stack,                      \
                    allocator->create_real(rad)); \
  native_ ## name ## _internal(stack);          \
}


// Basic Arithmetic
// The "divide by zero" error handling is done by
// the Object struct's operator overloads.
BINARY_MATH_OP(add, +);
BINARY_MATH_OP(sub, -);
BINARY_MATH_OP(mul, *);
BINARY_MATH_OP(div, /);
BINARY_MATH_OP(mod, %);
// abs, idiv, neg, exp: see manual implementations below

INTEGER_IDENTITY_FUNC(ceil, ceil);
INTEGER_IDENTITY_FUNC(floor, floor);
INTEGER_IDENTITY_FUNC(round, round);
INTEGER_IDENTITY_FUNC(trunc, trunc);

// Trigonometric functions
RANGED_FUNC(sqrt, sqrt, 0, HUGE_VAL);
RANGED_FUNC(ln, log, nextafter((double)0, (double)1), HUGE_VAL);
RANGED_FUNC(log, log10, nextafter((double)0, (double)1), HUGE_VAL);

// The `_internal` suffixed functions accept radians.
// The user-facing un-suffixed functions accept degrees.
RANGED_FUNC(sin_internal, sin, -HUGE_VAL, HUGE_VAL);
RANGED_FUNC(cos_internal, cos, -HUGE_VAL, HUGE_VAL);
RANGED_FUNC(atan_internal, atan, -HUGE_VAL, HUGE_VAL);
DEG_TO_RAD_WRAPPER(sin);
DEG_TO_RAD_WRAPPER(cos);
DEG_TO_RAD_WRAPPER(atan);


NATIVE(idiv) {
  REQUIRE_ARGS(stack, 2);
  auto b = (Object*) g_queue_pop_tail(stack);
  auto a = (Object*) g_queue_pop_tail(stack);

  int64_t divisor;
  int64_t dividend;
  int64_t result;

  if (a->type != Integer || b->type != Integer
      || a->data == nullptr || b->data == nullptr) {
    g_queue_push_tail(stack, allocator->create_exception());
    return;
  }

  divisor = b->i;
  dividend = a->i;

  if (dividend == 0) {
    g_queue_push_tail(stack, allocator->create_exception());
    return;
  }

  result = dividend / divisor;
  g_queue_push_tail(stack, allocator->create_int(result));
}

NATIVE(abs) {
  REQUIRE_ARGS(stack, 1);
  auto a = (Object*) g_queue_pop_tail(stack);

  if (a->type != Integer && a->type != Real) {
    g_queue_push_tail(stack, allocator->create_exception());
    return;
  }

  Object* ret;
  if (a->type == Integer) {
    int64_t val = labs(a->i);
    ret = allocator->create_int(val);
  } else if (a->type == Real) {
    double val = fabs(a->d);
    ret = allocator->create_real(val);
  } else {
    // Unreachable until we add new number type
    printf("native_abs: need to add a new number type\n");
    assert(false);
  }
  g_queue_push_tail(stack, ret);
}

NATIVE(exp) {
  REQUIRE_ARGS(stack, 2);
  auto power = (Object*) g_queue_pop_tail(stack);
  auto base = (Object*) g_queue_pop_tail(stack);

  double b, n;

  if (!is_num(power) || !is_num(base)) {
    g_queue_push_tail(stack, allocator->create_exception());
    return;
  }

  b = get_double(base);
  n = get_double(power);

  auto result = allocator->create_real(pow(b, n));
  g_queue_push_tail(stack, result);
}

NATIVE(neg) {
  REQUIRE_ARGS(stack, 1);
  auto a = (Object*) g_queue_pop_tail(stack);
  if (!is_num(a)) {
    g_queue_push_tail(stack, allocator->create_exception());
    return;
  }

  if (a->type == Integer) {
    a->i *= -1;
  } else if (a->type == Real) {
    a->d *= -1.0;
  }
}

NATIVE(rand) {
  const int32_t period = ((int64_t)1 << 31) - 1;

  int64_t out = labs(rand()) % period;
  auto output = allocator->create_int(out);
  g_queue_push_tail(stack, output);
}

NATIVE(srand) {
  REQUIRE_ARGS(stack, 1);

  auto seedObj = (Object*) g_queue_pop_tail(stack);

  if (seedObj->type != Integer) {
    fprintf(stderr, "native_srand: seed must be a Integer\n");
    g_queue_push_tail(stack, allocator->create_exception());
    return;
  }

  int64_t seed = seedObj->i;
  allocator->random_seed = seed;
  srand(seed);
}

NATIVE(rrand) {
  auto o = allocator->create_int(allocator->random_seed);
  g_queue_push_tail(stack, o);
}
