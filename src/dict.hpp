/*
 * dict.hpp - PostScript dictionary functions.
 *
 * © 2020 Cameron Conn
 */

#ifndef CS_DICT_H
#define CS_DICT_H

#include <map>
#include <string>

#include "memory.hpp"
#include "object.hpp"

PSDict* create_global_dict();
PSDict* create_system_dict();
PSDict* create_user_dict();

#endif
