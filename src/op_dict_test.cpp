/*
 * op_dict_test.cpp - PostScript dictionary operators.
 *
 * © 2020 Cameron Conn
 */

#include <glib.h>

#include "memory.hpp"
#include "op.hpp"
#include "object.hpp"

#include "gtest/gtest.h"

TEST(OpDict, Begin) {
  init_vm();
  GQueue *stk = g_queue_new();
  g_queue_push_tail(stk, allocator->create_dict());

  native_begin(stk);

  ASSERT_EQ(stk->length, 0);

  g_queue_free(stk);
}

TEST(OpDict, End) {
  init_vm();

  auto o = allocator->create_dict();
  g_ptr_array_add(allocator->dict_stack, o->dict);

  GQueue *stk = g_queue_new();

  native_end(stk);

  ASSERT_EQ(stk->length, 0);
  ASSERT_EQ(allocator->dict_stack->len, 3);

  native_end(stk);
  ASSERT_EQ(stk->length, 1);
  ASSERT_EQ(((Object*)g_queue_peek_tail(stk))->type, Exception);

  g_queue_free(stk);

}

TEST(OpDict, Dict) {
  init_vm();

  ASSERT_EQ(allocator->create_dict()->type, Dictionary);
}

TEST(OpDict, Def) {
  init_vm();

  const char *dne = "doesNotExist";

  auto o = allocator->lookup(dne);
  ASSERT_EQ(o, nullptr); // should not exist

  GQueue *stk = g_queue_new();

  g_queue_push_tail(stk, allocator->create_name_literal(dne));
  g_queue_push_tail(stk, allocator->create_boolean(true));

  native_def(stk);

  o = allocator->lookup(dne);

  ASSERT_NE(o, nullptr);
  ASSERT_EQ(((Object*)o)->b, true);

  g_queue_free(stk);
}
