/*
 * graphics.hpp - Basic Graphics Setup
 *
 * © 2020 Cameron Conn
 */

#ifndef CS_GRAPHICS_H
#define CS_GRAPHICS_H

#include <cstdint>

#include <cairo.h>

void cleanup_graphics();
bool setup_graphics();

cairo_matrix_t cairo_to_ps_matrix();

constexpr int pg_width = 612;
constexpr int pg_height = 792;
constexpr int pg_bpp = 32;

extern cairo_surface_t* cr_surface;
extern cairo_t *cr_context;

#endif
