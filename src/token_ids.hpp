/*
 * token_ids.h - Token ID definitions
 *
 * These token IDs are used when handling parsed input from the flex-generated
 * program in `lex.l`.
 *
 * Most of these are direct translations of the PostScript language's 
 * object types.
 *
 * © 2020 Cameron Conn
 */

#ifndef CS_TOKEN_IDS_H
#define CS_TOKEN_IDS_H

#define ID          256
#define PROC_BEG    257
#define PROC_END    258
#define COMMENT     260
#define METACOMMENT 261
#define ARRAY_BEG   262
#define ARRAY_END   263
#define NAME        264
#define NAME_LIT    265

#define STRING        300
#define STRING_NESTED 301
#define STRING_HEX    302
#define STRING_B85    303

#define INTEGER     310
#define REAL        311
#define RADIX       312

#define BOOL        315

#endif
