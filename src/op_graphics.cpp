/*
 * op_graphics.hpp - PostScript graphics operators.
 *
 * © 2020 Cameron Conn
 */

#include <cmath>
#include <cstdio>

#include <string>

#include <cairo.h>

#include "font.hpp"
#include "interpreter.hpp"
#include "memory.hpp"
#include "op.hpp"
#include "object.hpp"
#include "graphics.hpp"

#define ARE_NUMS(a, b)                          \
  ((a->type == Integer || a->type == Real)      \
   && (b->type == Integer || b->type == Real))

#define CAIRO_FUNCT(name, func)             \
NATIVE(name) {                              \
  if (cr_context == nullptr) return;        \
  func(cr_context);                         \
}

#define a allocator

#define REAL_POINT_FUNCT(name, func)        \
NATIVE(name) {                              \
  REQUIRE_ARGS(stack, 2);                   \
  auto y = (Object*) g_queue_pop_tail(stack);\
  auto x = (Object*) g_queue_pop_tail(stack);\
  double yd, xd;                            \
  if (!ARE_NUMS(x,y)) {                     \
    g_queue_push_tail(stack, a->create_exception()); \
    return;                                 \
  }                                         \
  switch (x->type) {                        \
  case Integer:                             \
    xd = (double)x->i;                      \
    break;                                  \
  case Real:                                \
    xd = x->d;                              \
    break;                                  \
  default:                                  \
    g_queue_push_tail(stack, a->create_exception()); \
    return;                                 \
  }                                         \
  switch (y->type) {                        \
  case Integer:                             \
    yd = (double)y->i;                      \
    break;                                  \
  case Real:                                \
    yd = y->d;                              \
    break;                                  \
  default:                                  \
    g_queue_push_tail(stack, a->create_exception()); \
    return;                                 \
  }                                         \
  func(cr_context, xd, yd);                 \
}


REAL_POINT_FUNCT(lineto, cairo_line_to)
REAL_POINT_FUNCT(moveto, cairo_move_to)
REAL_POINT_FUNCT(rlineto, cairo_rel_line_to)
REAL_POINT_FUNCT(rmoveto, cairo_rel_move_to)

CAIRO_FUNCT(newpath, cairo_new_path)
CAIRO_FUNCT(closepath, cairo_close_path)
CAIRO_FUNCT(fill, cairo_fill)
CAIRO_FUNCT(stroke, cairo_stroke)

NATIVE(setgray) {
  REQUIRE_ARGS(stack, 1);
  auto o = (Object*) g_queue_pop_tail(stack);
  if (o->type != Integer && o->type != Real) {
    g_queue_push_tail(stack, a->create_exception());
  } else {
    // Don't need to clamp, this is already clamped
    // by Cairo.
    double rgb = get_double(o);
    cairo_set_source_rgb(cr_context, rgb, rgb, rgb);
  }
}

NATIVE(currentgray) {
  double r, g, b, alpha;
  cairo_pattern_t* src = cairo_get_source(cr_context);

  cairo_pattern_get_rgba(src, &r, &g, &b, &alpha);

  double gray = (r + g + b) / 3.0;
  auto o = a->create_real(gray);
  g_queue_push_tail(stack, o);
}

NATIVE(setlinewidth) {
  REQUIRE_ARGS(stack, 1);
  auto o = (Object*) g_queue_pop_tail(stack);
  if (o->type != Integer && o->type != Real) {
    g_queue_push_tail(stack, a->create_exception());
  } else {
    double width = get_double(*o);
    cairo_set_line_width(cr_context, width);
  }
}

NATIVE(currentlinewidth) {
  double width = cairo_get_line_width(cr_context);
  auto o = a->create_real(width);
  g_queue_push_tail(stack, o);
}

// TODO: Instead of this, we should actually create a new Cairo context
// and switch to it.
NATIVE(showpage) {
  if (cr_surface != nullptr)
    cairo_surface_flush(cr_surface);
}

NATIVE(show) {
  REQUIRE_ARGS(stack, 1);
  auto textObj = (Object*) g_queue_pop_tail(stack);

  if ((textObj->type != String)
      || (textObj->str == nullptr)) {
    g_queue_push_tail(stack, a->create_exception());
    return;
  }

  std::string* txt = textObj->str->str;
  show_text(*txt);
}

NATIVE(curveto) {
  REQUIRE_ARGS(stack, 6);

  auto f = (Object*) g_queue_pop_tail(stack);
  auto e = (Object*) g_queue_pop_tail(stack);
  auto d = (Object*) g_queue_pop_tail(stack);
  auto c = (Object*) g_queue_pop_tail(stack);
  auto b = (Object*) g_queue_pop_tail(stack);
  auto _a = (Object*) g_queue_pop_tail(stack);

  if (!is_num(_a) || !is_num(b) || !is_num(c)
      || !is_num(d) || !is_num(e) || !is_num(f)) {
    g_queue_push_tail(stack, a->create_exception());
    return;
  }

  cairo_curve_to(cr_context,
                 get_double(_a),
                 get_double(b),
                 get_double(c),
                 get_double(d),
                 get_double(e),
                 get_double(f));
}

NATIVE(arc) {
  REQUIRE_ARGS(stack, 5);

  auto f = (Object*) g_queue_pop_tail(stack);
  auto e = (Object*) g_queue_pop_tail(stack);
  auto d = (Object*) g_queue_pop_tail(stack);
  auto c = (Object*) g_queue_pop_tail(stack);
  auto b = (Object*) g_queue_pop_tail(stack);

  if (!is_num(b) || !is_num(c) || !is_num(d)
      || !is_num(e) || !is_num(f)) {
    g_queue_push_tail(stack, a->create_exception());
    return;
  }

  cairo_arc(cr_context,
            get_double(b),
            get_double(c),
            get_double(d),
            get_double(e) * M_PI / 180,
            get_double(f) * M_PI / 180);
}
