/*
 * op.cpp - Native stack-oriented PostScript operators.
 *
 * © 2020 Cameron Conn
 */

#include <glib.h>

#include "interpreter.hpp"
#include "object.hpp"
#include "op.hpp"

NATIVE(clear) {
  g_queue_clear(stack);
}

NATIVE(cleartomark) {
  int mark = -1;

  for (int i = stack->length - 1; i >= 0; i--) {
    if (((Object*)g_queue_peek_nth(stack, i))->type == Mark) {
      mark = i;
      break;
    }
  }

  if (mark < 0) {
    auto e = allocator->create_exception();
    g_queue_push_tail(stack, e);
    return;
  }

  while (stack->length > mark)
    g_queue_pop_tail(stack);
}

NATIVE(count) {
  g_queue_push_tail(stack, allocator->create_int(stack->length));
}

NATIVE(counttomark) {
  int count = 0;
  bool found_mark = false;

  for (int i = stack->length - 1; i >= 0; i--) {
    if (((Object*)g_queue_peek_nth(stack, i))->type == Mark) {
      found_mark = true;
      break;
    } else {
      count++;
    }
  }

  Object* output;
  if (found_mark) {
    output = allocator->create_int(count);
  } else {
    output = allocator->create_exception();
  }
  g_queue_push_tail(stack, output);
}

NATIVE(copy) {
  // Special case for `0 copy` to work.
  if (stack->length == 1 &&
      ((Object*)g_queue_peek_tail)->type == Integer &&
      ((Object*)g_queue_peek_tail)->i == 0) {
    g_queue_pop_tail(stack);
    return;
  }

  // Make sure we have at least 2 args as required for
  // the rest of the possible definitions of `copy`.
  REQUIRE_ARGS(stack, 2);

  auto sz = stack->length;
  auto top = (Object*) g_queue_peek_nth(stack, sz-1);
  auto base = (Object*) g_queue_peek_nth(stack, sz-2);

  // TODO: LL 1 -> Check if sizes of copy destinations are correct.
  // TODO: LL 1/2/3 -> Appropriately handle attribute copying, pg. 548
  switch(top->type) {
  case Array:
    if (base->type != Array) {
      g_queue_push_tail(stack, allocator->create_exception());
    } else {
      if (top->arr->arr != nullptr)
        g_ptr_array_free(top->arr->arr, TRUE);

      GPtrArray *other = base->arr->arr;
      top->arr->arr = g_ptr_array_copy(other, nullptr, nullptr);
    }
    break;

  case Dictionary:
    if (base->type != Dictionary) {
      g_queue_push_tail(stack, allocator->create_exception());
    } else {
      const std::map<string,Object*>& other = *base->dict->dict;
      *top->dict->dict = other;
    }
    break;

  case Integer: {
    auto toCopy = top->i;
    auto size = stack->length - 1; // Excludes last element.

    if (toCopy <= size && toCopy >= 0) {
      g_queue_pop_tail(stack);
      for (int i = size - toCopy; i < size; i++) {
        auto o = (Object*) g_queue_peek_nth(stack, i);
        g_queue_push_tail(stack, o);
      }
    } else {
      // rangecheck
      g_queue_push_tail(stack, allocator->create_exception());
    }
  }
    break;

  case Save:
    printf("Unimplemented: copy Save\n");
    g_queue_push_tail(stack, allocator->create_exception());
    break;

  case String: {
    if (base->type != String) {
      g_queue_push_tail(stack, allocator->create_exception());
      return;
    }

    const string& other = *base->str->str;
    *top->str->str = other;
    break;
  }

  default:
    g_queue_push_tail(stack, allocator->create_exception());
    break;
  }
}

NATIVE(dup) {
  REQUIRE_ARGS(stack, 1);

  auto last = (Object*) g_queue_peek_tail(stack);
  g_queue_push_tail(stack, allocator->clone_object(last));
}

NATIVE(exch) {
  REQUIRE_ARGS(stack, 2);

  auto last1 = (Object*) g_queue_pop_tail(stack);
  auto last2 = (Object*) g_queue_pop_tail(stack);
  g_queue_push_tail(stack, last1);
  g_queue_push_tail(stack, last2);
}

NATIVE(index) {
  if (stack->length < 2) {
    // stack not large enough
    g_queue_push_tail(stack, allocator->create_exception());
    return;
  }

  auto top = (Object*) g_queue_peek_tail(stack);
  if (top->type != Integer || get_int(top) < 0) {
    //fprintf(stderr, "n for index must be an integer >= 0");
    g_queue_push_tail(stack, allocator->create_exception());
    return;
  }

  int index = top->i;
  int size = stack->length - 2; // -2 excludes the current top element.

  if (index > size) {
    //fprintf(stderr, "n for index must be less than the size of the stack");
    g_queue_push_tail(stack, allocator->create_exception());
    return;
  }

  g_queue_pop_tail(stack);
  auto target = (Object*) g_queue_peek_nth(stack, size - index);
  g_queue_push_tail(stack, target);
}

NATIVE(mark) {
  g_queue_push_tail(stack, allocator->create_mark());
}

NATIVE(pop) {
  REQUIRE_ARGS(stack, 1);

  g_queue_pop_tail(stack);
}

NATIVE(print) {
  REQUIRE_ARGS(stack, 1);

  auto o = (Object*) g_queue_pop_tail(stack);

  print_object_newline(*o);
}

NATIVE(print_stack) {
  g_queue_reverse(stack);

  auto print = [](gpointer data, gpointer user_data) {
                 auto i = (Object*) data;
                 print_object_newline(*i);
               };
  g_queue_foreach(stack, print, nullptr);

  g_queue_reverse(stack);
}

NATIVE(roll) {
  REQUIRE_ARGS(stack, 2);

  const size_t size = stack->length;
  const int elems = stack->length - 2;
  const auto j = (Object*) g_queue_peek_nth(stack, size-1);
  const auto n = (Object*) g_queue_peek_nth(stack, size-2);

  if (j->type != Integer
      || n->type != Integer
      || n->i < 0
      || n->i > elems) {
    g_queue_push_tail(stack, allocator->create_exception());
    return;
  }

  const int num = n->i;
  const int shift = - j->i;

  const int endIndex = elems; // exclusive
  const int beginIndex = endIndex - num; // inclusive

  GPtrArray *tmp = g_ptr_array_new(); // Object*
  for (int i = beginIndex; i < endIndex; i++) {
    auto stkItem = (Object*) g_queue_peek_nth(stack, i);
    g_ptr_array_add(tmp, stkItem);
  }

  for (int i = beginIndex; i < endIndex; i++) {
    int k = (i + shift) % num;
    while (k < 0) k += num; // force k to be positive

    GList *link = g_queue_peek_nth_link(stack, i);
    link->data = (Object*) g_ptr_array_index(tmp, k);
  }

  g_ptr_array_free(tmp, TRUE);

  g_queue_pop_tail(stack);
  g_queue_pop_tail(stack);
}
