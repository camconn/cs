/*
 * font.cpp - Font Utilities
 *
 * © 2020 Cameron Conn
 */


#include <cassert>
#include <cstdint>
#include <cstdio>

#include <string>

#include <glib.h>

#include <cairo.h>
#include <cairo/cairo-ft.h>

#include <ft2build.h>
#include FT_FREETYPE_H
#include FT_ERRORS_H

#include <fontconfig/fontconfig.h>

#include "font.hpp"
#include "graphics.hpp"
#include "matrix.hpp"
#include "memory.hpp"


FT_Library library;
FT_Face face;

// How font mappings work:
// FontID -> cairo_scaled_font_t
// cairo_font_face_t <--> FT_Face

GPtrArray *scaled_fonts;    // holds cairo_scaled_font_t*
GPtrArray *font_faces;      // holds cairo_font_face_t*
GPtrArray *ft_faces;        // holds FT_Face


std::string get_font_path(std::string name) {
  FcFontSet*    fs;
  FcPattern*    pattern;
  FcResult      result;
  std::string   path;

  FcChar8* cname = (FcChar8*) name.c_str();
  FcInit();

  pattern = FcNameParse((FcChar8*)cname);
  if (!pattern)
    return nullptr;

  FcConfigSubstitute(0, pattern, FcMatchPattern);
  FcDefaultSubstitute(pattern);

  fs = FcFontSetCreate();
  if (!fs) {
    fprintf(stderr, "could not obtain FcFontSet\n");
    return nullptr;
  }

  FcPattern* match;
  match = FcFontMatch(0, pattern, &result);
  if (match) {
      FcFontSetAdd(fs, match);
  }
  FcPatternDestroy(pattern);

	FcChar8* format = (FcChar8 *) "%{=fcmatch}\n";

  if (fs) {
    int j;
    FcObjectSet* objs = FcObjectSetBuild(FC_FAMILY, FC_STYLE, FC_FILE, (char*)0);

    if (fs->nfont > 0) {
      FcPattern *font;
      FcValue v;

      font = FcPatternFilter (fs->fonts[0], objs);

      FcPatternGet(font, FC_FILE, 0, &v);
      const char* filepath = (char*)v.u.f;
      //printf("path: %s\n", filepath);
      path = std::string(filepath);

      FcPatternDestroy(font);
    }

    FcFontSetDestroy(fs);
    FcObjectSetDestroy(objs);
  } else {
    fprintf(stderr, "could not obtain FcFontSet\n");
  }

  return path;
}

int max(int a, int b) {
  if (a > b) {
    return a;
  }
  return b;
}

cairo_font_face_t* get_font_face(std::string fname) {
  if (library == nullptr) {
    if (!setup_fonts()) {
      return nullptr;
    }
  }

  std::string name(fname);

  int fp = max(name.find("-Bold"), name.find("-bold"));
  if (fp > -1)
    name.replace(fp, 5, ":bold");

  fp = max(name.find("-oblique"), name.find("-oblique"));
  if (fp > -1)
    name.replace(fp, 8, ":oblique");

  fp = max(name.find("-italic"), name.find("-Italic"));
  if (fp > -1)
    name.replace(fp, 7, ":italic");


  std::string path = get_font_path(name);
  FT_Error error;
  FT_Face tmp_face;
  error = FT_New_Face(library,
                      path.c_str(),
                      0,
                      &tmp_face);

  if (error == FT_Err_Unknown_File_Format) {
    fprintf(stderr, "Could not load font: %s\n", FT_Error_String(error));
    return nullptr;
  } else if (error) {
    fprintf(stderr, "An Unknown FreeType error occurred: %s\n", FT_Error_String(error));
    return nullptr;
  }

  g_ptr_array_add(ft_faces, tmp_face);
  FT_Face n_face = tmp_face;
  cairo_font_face_t* f;
  f = cairo_ft_font_face_create_for_ft_face(n_face, 0);
  if (f == nullptr) {
    fprintf(stderr, "Could not create FreeType face\n");
    exit(99);
  }

  return f;
}

int64_t find_font(const char* name) {
  cairo_font_face_t *face;
  cairo_font_options_t *opts;
  cairo_matrix_t ctm, font_mat;
  cairo_scaled_font_t* scaled;

  face = get_font_face(name);
  if (face == nullptr)
    return -1;

  opts = cairo_font_options_create();
  if (opts == nullptr)
    return -2;

  cairo_matrix_init_identity(&font_mat);
  cairo_matrix_scale(&font_mat, 1, 1);
  reflect_y(&font_mat);
  cairo_matrix_init_identity(&ctm);

  scaled = cairo_scaled_font_create(face, &font_mat, &ctm, opts);

  cairo_font_options_destroy(opts);

  int64_t id = scaled_fonts->len;
  g_ptr_array_add(scaled_fonts, scaled);
  return id;
}

bool setup_fonts() {
  if (library != nullptr) {
    fprintf(stderr, "Already called setup_fonts!");
    return false;
  }

  scaled_fonts = g_ptr_array_new();
  font_faces = g_ptr_array_new();
  ft_faces = g_ptr_array_new();

  FT_Error error = FT_Init_FreeType(&library);
  if (error) {
    fprintf(stderr, "Could not setup fonts: %s\n", FT_Error_String(error));
    return false;
  }

  int64_t id = find_font("Helvetica");
  if (id < 0)
    assert(false);

  int64_t scaled = scale_font(id, 12);
  if (scaled < 0)
    assert(false);

  use_font(scaled);

  return true;
}

void cleanup_fonts() {
  auto f = [](gpointer data, gpointer user_data) {
             cairo_scaled_font_destroy((cairo_scaled_font_t*)data);
           };
  g_ptr_array_foreach(scaled_fonts, f, nullptr);
  g_ptr_array_free(scaled_fonts, TRUE);

  auto g = [](gpointer data, gpointer user_data) {
             cairo_font_face_destroy((cairo_font_face_t*)data);
           };
  g_ptr_array_foreach(font_faces, g, nullptr);
  g_ptr_array_free(font_faces, TRUE);


  auto h = [](gpointer data, gpointer user_data) {
             FT_Done_Face((FT_Face) data);
           };
  g_ptr_array_foreach(ft_faces, h, nullptr);
  g_ptr_array_free(ft_faces, TRUE);
}

// use font `fontID`
// Returns true if font is changed successfully.
// Returns false on failure.
bool use_font(int64_t fontID) {
  // Check if `fontID` is a valid font id
  if (fontID < 0 || fontID >= scaled_fonts->len) {
    fprintf(stderr, "invalid font id: %ld\n", fontID);
    return false;
  }

  //cairo_scaled_font_t* sc = scaled_fonts[fontID];
  cairo_scaled_font_t* sc = (cairo_scaled_font_t*) g_ptr_array_index(scaled_fonts, fontID);
  cairo_set_scaled_font(cr_context, sc);
  return true;
}

int64_t scale_font(int64_t src_id, double scale) {
  cairo_scaled_font_t* to_scale =
    (cairo_scaled_font_t*) g_ptr_array_index(scaled_fonts, src_id);
  cairo_scaled_font_t* scaled;
  cairo_font_options_t *opts;
  cairo_matrix_t ctm, font_mat;

  cairo_font_face_t* baseFace = cairo_scaled_font_get_font_face(to_scale);
  cairo_scaled_font_get_ctm(to_scale, &ctm);
  cairo_scaled_font_get_font_matrix(to_scale, &font_mat);
  cairo_matrix_scale(&font_mat, scale, scale);

  cairo_scaled_font_get_font_options(to_scale, opts);
  if (opts == nullptr)
    opts = cairo_font_options_create();

  scaled = cairo_scaled_font_create(baseFace, &font_mat, &ctm, opts);
  cairo_font_options_destroy(opts);

  int64_t id = scaled_fonts->len;
  g_ptr_array_add(scaled_fonts, scaled);
  return id;
}

// use font `name` with default scale
// Returns the fontID of the new font.
// Returns < 0 when an error occurs.
int64_t make_scaled_font(int64_t prev_id, double factor) {
  if (cr_context == nullptr) {
    fprintf(stderr, "cr_context must be initialized when using make_scaled_font\n");
    return -1;
  }

  cairo_scaled_font_t *sc = (cairo_scaled_font_t*) g_ptr_array_index(scaled_fonts, prev_id);
  if (sc == nullptr)
    return -2;

  cairo_font_face_t *f = cairo_scaled_font_get_font_face(sc);
  if (f == nullptr)
    return -3;


  cairo_matrix_t ft_matrix;
  cairo_scaled_font_get_font_matrix(sc, &ft_matrix);
  cairo_matrix_scale(&ft_matrix, factor, factor);

  cairo_matrix_t ft_ctm;
  cairo_scaled_font_get_ctm(sc, &ft_ctm);

  cairo_font_options_t *ft_opts;
  cairo_scaled_font_get_font_options(sc, ft_opts);
  if (ft_opts == nullptr)
    ft_opts = cairo_font_options_create();

  cairo_scaled_font_t *scaled = cairo_scaled_font_create(f, &ft_matrix, &ft_ctm, ft_opts);

  g_ptr_array_add(scaled_fonts, scaled);
  return scaled_fonts->len - 1;
}

void show_text(const std::string& text) {
  cairo_text_cluster_t *clusters = nullptr;
  cairo_glyph_t* glyphs = nullptr;
  int n_clusters;
  int n_glyphs;
  cairo_text_cluster_flags_t flags;

  cairo_status_t status;
  cairo_scaled_font_t* scaled;
  double x, y;

  cairo_get_current_point(cr_context, &x, &y);
  scaled = cairo_get_scaled_font(cr_context);

  status = cairo_scaled_font_text_to_glyphs (
    scaled,
    x,
    y,
    text.c_str(),
    text.size(),
    &glyphs,
    &n_glyphs,
    &clusters,
    &n_clusters,
    &flags);

  if (status == CAIRO_STATUS_SUCCESS) {
    cairo_show_text_glyphs(cr_context,
                           text.c_str(),
                           text.size(),
                           glyphs,
                           n_glyphs,
                           clusters,
                           n_clusters,
                           flags);

    cairo_glyph_free(glyphs);
    cairo_text_cluster_free(clusters);
  } else {
    fprintf(stderr, "Could not show text, "
                    "error: %s\n", cairo_status_to_string(status));
  }
}

