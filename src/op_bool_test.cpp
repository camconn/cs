/*
 * op_bool_test.cpp - Boolean & Logical PostScript operators
 *
 * © 2020 Cameron Conn
 */

#include <glib.h>

#include "memory.hpp"
#include "op.hpp"
#include "object.hpp"

#include "gtest/gtest.h"

TEST(OpBool, eq) {
  init_vm();

  GQueue *stk = g_queue_new();

  g_queue_push_tail(stk, allocator->create_int(5));
  g_queue_push_tail(stk, allocator->create_real(5.0));
  native_eq(stk);
  ASSERT_EQ(*((Object*)g_queue_peek_tail(stk)), *allocator->create_boolean(true));
  g_queue_pop_tail(stk);

  g_queue_push_tail(stk, allocator->create_int(67));
  g_queue_push_tail(stk, allocator->create_real(68));
  native_eq(stk);
  ASSERT_NE(*((Object*)g_queue_peek_tail(stk)), *allocator->create_boolean(true));
  g_queue_pop_tail(stk);

  g_queue_free(stk);
}

TEST(OpBool, lt) {
  init_vm();

  GQueue *stk = g_queue_new();

  auto t = allocator->create_boolean(true);

  g_queue_push_tail(stk, allocator->create_int(5));
  g_queue_push_tail(stk, allocator->create_real(9.0));
  native_lt(stk);
  ASSERT_EQ(*((Object*)g_queue_peek_tail(stk)), *t);
  g_queue_clear(stk);

  g_queue_push_tail(stk, allocator->create_int(5));
  g_queue_push_tail(stk, allocator->create_real(6));
  native_lt(stk);
  ASSERT_EQ(*((Object*)g_queue_peek_tail(stk)), *t);
  g_queue_clear(stk);

  g_queue_push_tail(stk, allocator->create_int(67));
  g_queue_push_tail(stk, allocator->create_real(68));
  native_lt(stk);
  ASSERT_EQ(*((Object*)g_queue_peek_tail(stk)), *t);
  g_queue_pop_tail(stk);

  g_queue_free(stk);
}

