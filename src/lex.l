/*
 * lex.l - cps lexical scanner generator
 *
 * Licensed AGPL v3 or any later version.
 *
 * © 2020 Cameron Conn <cam at camconn dot cc>
 */

%option noyywrap
%option stack


%{
#include <cstdio>

#include "token_ids.hpp"
%}


/* Whitespace per PLRM */
whitespace  [\0\x09\x0A\x0C\x0D\x20]+
lpar        \(
rpar        \)
lbr         \{
rbr         \}
lsq         \[
rsq         \]

/* Literal boolean values */
bool        true|false
        
/* Stuff that is safe to put in a string */
oct         \\[0-9]{2,3}
escaped     \\[^0-9]
normal      [A-Za-z0-9 \-!@#\$%\^&*_+={}\[\]\|:;'"<>,./?]+

namelitsafe [\/A-Za-z0-9`!@\#\$\^&\*\-\+\=\|\:\;'"\0]
namesafe    [A-Za-z0-9`!@\#\$\^&\*\-\+\=\|\:\;'"\0]

/* Exclusive states */
%x X_COMMENT X_METACOMMENT X_STRNG X_STRNG_HEX X_STRNG_B85

%%

{whitespace}+           { ; }

 /* Comments and metacomments */
\%\%                    { yy_push_state(X_METACOMMENT); }
\%                      { yy_push_state(X_COMMENT); }
<X_COMMENT>.*\n         { yy_pop_state(); return COMMENT; }
<X_METACOMMENT>.*\n     { yy_pop_state(); return METACOMMENT; }

 /* Arrays and procedures */
{lbr}                   { return PROC_BEG; }
{rbr}                   { return PROC_END; }
{lsq}                   { return ARRAY_BEG; }
{rsq}                   { return ARRAY_END; }

 /* Strings */
<INITIAL>\<[A-Fa-f0-9]+\>       { return STRING_HEX; }
<INITIAL>\<\~[A-Za-z0-9]+~\>    { return STRING_B85; }

 /* We have to do the below nasty hack because RE's don't support */
 /* constraints such as nested parentheses. Instead, we are using */
 /* recursion of flex's states to match parentheses when we need */
 /* them to be matched. */
<INITIAL,X_STRNG>{lpar}         { yy_push_state(X_STRNG); yymore(); }
<X_STRNG>({normal}|{oct}|{escaped}|{whitespace})+  {
    yymore();
    return STRING_NESTED;
}

<X_STRNG>{rpar} {
    if (yy_top_state() == X_STRNG) {
        yy_pop_state();
        yymore();
        return STRING_NESTED;
    } else {
        yy_pop_state();
        return STRING;
    }
}

 /* Digits */
[-+]?[0-9]+                         { return INTEGER; }
[-+]?[0-9]*\.?[0-9]+([Ee]-?[0-9]+)? { return REAL; }
[0-3]?[0-9]\#[0-9A-Za-z]+           { return RADIX; }

 /* Boolean followed by a delimiter*/
{bool}/({whitespace}|{lpar}|{rpar}|{lbr}|{rbr}|{lsq}|{rsq}) { return BOOL; }

 /* IDs and names */
\/{namesafe}*                   { return NAME_LIT; }
{namelitsafe}{namesafe}*        { return NAME; }

 /* Error Handling and EOF */
.                       { printf("UNRECOGNIZED: %s\n", yytext); }
<<EOF>>                 { return EOF; }

%%
