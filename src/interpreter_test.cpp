/*
 * interpreter_test.cpp - Test file for interpreter
 *
 * © 2020 Cameron Conn
 */

#include <string>

#include <glib.h>

#include "gtest/gtest.h"

#include "interpreter.hpp"
#include "memory.hpp"
#include "object.hpp"
#include "object_test.hpp"

using std::string;

#define a allocator

TEST(Interpreter, TypicalMath) {
    init_vm();

    Stack p = g_queue_new(),
      s = g_queue_new();

    g_queue_push_tail(p, a->create_int(3));
    g_queue_push_tail(p, a->create_int(4));
    g_queue_push_tail(p, a->create_name("add"));
    g_queue_push_tail(p, a->create_int(4));
    g_queue_push_tail(p, a->create_int(2));
    g_queue_push_tail(p, a->create_name("div"));
    g_queue_push_tail(p, a->create_name("mul"));

    int ret = execute(s, p);

    ASSERT_EQ(ret, 0);
    ASSERT_EQ(*((Object*)g_queue_peek_tail(s)),
              *a->create_int(14));
}

TEST(Interpreter, UserProcedures) {
  init_vm();

  GPtrArray *toInchSrc = g_ptr_array_new(); // Object*
  g_ptr_array_add(toInchSrc, a->create_int(72));
  g_ptr_array_add(toInchSrc, a->create_name("mul"));

  PSArray *arr = a->create_gc_array(toInchSrc);

  auto toInch = a->create_array(arr);
  toInch->attribs[ExecutableBit] = 1;

  Stack p = g_queue_new(),
    s = g_queue_new();

  g_queue_push_tail(p, a->create_name_literal("inch"));
  g_queue_push_tail(p, toInch);
  g_queue_push_tail(p, a->create_name("def"));

  g_queue_push_tail(p, a->create_int(1));
  g_queue_push_tail(p, a->create_name("inch"));
  g_queue_push_tail(p, a->create_int(4));
  g_queue_push_tail(p, a->create_name("inch"));

  int ret = execute(s, p);

  ASSERT_EQ(ret, 0);

  ASSERT_EQ(*((Object*)g_queue_peek_head(s)),
            *a->create_int(72));
  ASSERT_EQ(*((Object*)g_queue_peek_tail(s)),
            *a->create_int(288));

  g_queue_free(s);
  g_queue_free(p);
}
