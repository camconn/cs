/*
 * object_test.cpp - Test Object Stuff
 *
 * © 2020 Cameron Conn
 */

#include <cmath>
#include <cstdint>

#include <glib.h>

#include "gtest/gtest.h"

#include "object.hpp"
#include "object_test.hpp"
#include "op.hpp"
#include "memory.hpp"

#define a_ allocator

TEST(Object, NumEquals) {
  init_vm();

  auto three = a_->create_int(3);
  auto six = a_->create_int(6);
  auto also_six = a_->create_real(6);
  auto seven = a_->create_real(7);

  ASSERT_NE(*three, *seven);
  ASSERT_NE(*six, *seven);
  ASSERT_EQ(*six, *also_six);
  ASSERT_EQ(*also_six, *six);
}

TEST(Object, ArrayEquals) {
  init_vm();

  GPtrArray *arr1 = g_ptr_array_new();
  GPtrArray *arr2 = g_ptr_array_new();

  g_ptr_array_add(arr1, a_->create_int(6));
  g_ptr_array_add(arr2, a_->create_int(6));

  auto a1 = a_->create_gc_array(arr1);
  auto a2 = a_->create_gc_array(arr2);

  auto o1 = a_->create_array(a1);
  auto o2 = a_->create_array(a2);

  ASSERT_EQ(*o1, *o2);
  g_ptr_array_free(arr1, TRUE);
  g_ptr_array_free(arr2, TRUE);
}

TEST(Object, StringEquals) {
  init_vm();

  auto a = allocator->create_string("abcd");
  auto b = allocator->create_string("abcd");

  ASSERT_EQ(*a, *b);
}


TEST(Object, NameEquals) {
  init_vm();

  auto a = allocator->create_name("abcd");
  auto b = allocator->create_name_literal("abcd");
  auto c = allocator->create_name_literal("defg");

  ASSERT_EQ(*a, *b);
  ASSERT_NE(*a, *c);
  ASSERT_NE(*b, *c);
}


TEST(Object, MixedEquals) {
  init_vm();

  auto a = allocator->create_name("abcd");
  auto b = allocator->create_string("abcd");
  auto c = allocator->create_name_literal("abcd");
  ASSERT_EQ(*a, *b);
  ASSERT_EQ(*b, *c);
  ASSERT_EQ(*a, *c);

  auto d = allocator->create_name("efgh");
  auto e = allocator->create_string("efgh");
  auto f = allocator->create_name_literal("efgh");
  ASSERT_NE(*a, *d);
  ASSERT_NE(*a, *e);
  ASSERT_NE(*a, *f);
  ASSERT_NE(*b, *d);
  ASSERT_NE(*b, *e);
  ASSERT_NE(*b, *f);
  ASSERT_NE(*c, *d);
  ASSERT_NE(*c, *e);
  ASSERT_NE(*c, *f);
}
