/*
 * util.cpp - Common Utilities
 *
 * © 2020 Cameron Conn
 */

#include "util.hpp"

bool str_equals(const char* a, const char* b) {
  if (a == nullptr || b == nullptr)
    return false;

  while (*a != 0 && *b != 0) {
    if (*a++ != *b++)
      return false;
  }

  return (*a == *b);
}
