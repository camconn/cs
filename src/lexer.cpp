/*
 * lexer.cpp - Interface to flex for CS
 *
 * © 2020 Cameron Conn
 */


#include <cassert>
#include <cstdio>
#include <cstdlib>
//#include <cstring>

#include <sstream>
#include <string>

#include "lex.yy.h"

#include "lexer.hpp"
#include "memory.hpp"
#include "object.hpp"
#include "token_ids.hpp"
#include "util.hpp"


using std::string;
using std::printf;

#define STUB(name) \
  case name: \
    printf("need to implement %s: %s\n", #name, yytext); \
    assert(false); \
    break;


// Translate a PostScript string with escape codes into a
// string with those escape codes replaced.
string translate_escaped_string(string& input) {
  size_t len = input.size();
  //printf("input: %s; size: %d\n", input.c_str(), input.size());
  std::ostringstream translation;

  char current, peek;
  peek = input[0];
  for (size_t i = 0; i < len; i++) {
    //current = peek;
    current = input[i];
    if (i < len - 1)
      peek = input[i+1];

    switch(current) {
    case '\\':  // if we find a backslash
      switch (peek) {
      // Named escape codes
      case 'n': translation << '\n'; i += 1; continue;
      case 'r': translation << '\r'; i += 1; continue;
      case 't': translation << '\t'; i += 1; continue;
      case 'b': translation << '\b'; i += 1; continue;
      case 'f': translation << '\f'; i += 1; continue;

      // Ignore newline
      case '\n': i += 1; continue;
      case '\r':
        // check if sequence is CR-LF
        if (i < len - 2 && input[i+2] == '\n') {
          i += 2;
        } else {
          i += 1;
        }
        continue;

      // Literals
      case '\\':
      case '(':
      case ')':
          translation << peek; i+= 1; continue;

      // Handle octal digits
      case '0':
      case '1':
      case '2':
      case '3':
      case '4':
      case '5':
      case '6':
      case '7':
      case '8':
      case '9':
        if (i + 2 < len && isdigit(input[i+2])) {
          char *waste;
          int value;

          // We have at least a 2 digit octal, do we have 3 digits?
          if (i + 3 < len && isdigit(input[i+3])) {
            // We have a 3 digit octal
            char buf[4];
            buf[0] = peek;
            buf[1] = input[i+2];
            buf[2] = input[i+3];
            buf[3] = '\0';
            value = std::strtoul(buf, &waste, 8);
            i += 3;
          } else {
            // We have a 2 digit octal
            char buf[3];
            buf[0] = peek;
            buf[1] = input[i+2];
            buf[2] = '\0';
            value = std::strtoul(buf, &waste, 8);
            i += 2;
          }

          // Take translated value and add it to translated buffer
          translation << (char) value;
          continue;
        } else {
          goto default_;
        }
        continue;

      default_:
      default:
        // Ignore backslash
        i += 1;
        translation << current;
        continue;
      }

    case '\r':
      if (peek == '\n') {
        // Skip next character to translate CR-LF to LF
        i += 1;
      }
      current = '\n';
      // fall through
    default:  // no backslash, just append
      translation << current;
      continue;
    }
  }

  return translation.str();
}

string translate_hex_string(string& input) {
  std::ostringstream translation;

  if (input.size() % 2 != 0) {
    printf("translate_hex_string only takes inputs of even length\n");
    assert(false);
  }

  char *waste;
  char buf[3];
  buf[2] = 0;
  char value;
  for (size_t i = 0; i + 1 < input.size(); i += 2) {
    buf[0] = input[i];
    buf[1] = input[i+1];

    value = strtol(buf, &waste, 16);
    translation << value;
  }

  return translation.str();
}


int lexer(Stack program, FILE *input) {
  int ret;
  yyin = input;

	while((ret = yylex()) != EOF) {
    switch (ret) {

    // Comment Handling
    case METACOMMENT:
      // TODO: Extract metadata from meta-comments

      // fall through
    case COMMENT:
      break;

    // Procedure and arrays
    case ARRAY_BEG:
    case PROC_BEG: {
      auto o = allocator->create_mark();

      if (ret == PROC_BEG)
        o->attribs[ExecutableBit] = 1;

      g_queue_push_tail(program, o);
      break;
    }

    case ARRAY_END:
    case PROC_END: {
      auto o = allocator->create_mark_end();

      if (ret == PROC_END)
        o->attribs[ExecutableBit] = 1;

      g_queue_push_tail(program, o);
      break;
    }

    // Numbers
    case INTEGER: {
      // TODO: Convert to real when too large for integers
      auto o = allocator->create_int(yytext);
      g_queue_push_tail(program, o);
      break;
    }
    case REAL: {
      // TODO: Limitcheck when out of range
      auto o = allocator->create_real(yytext);
      g_queue_push_tail(program, o);
      break;
    }
    case RADIX: {
      // TODO: Limitcheck when out of range
      const char *front_stop = strchr(yytext, '#');
      const char *back_start = strrchr(yytext, '#');
      char *waste;
      int64_t value, base;

      if (back_start - yytext > yyleng) {
        // Prevent read OOB with yytext
        printf("May attempt to read OOB. Failing fast\n");
        assert(false);
      }

      string front_str(yytext, front_stop - yytext);
      string back_str(back_start + 1);

      base = strtoll(front_str.c_str(), &waste, 10);
      value = strtoll(back_str.c_str(), &waste, base);

      auto o = allocator->create_int(value);
      g_queue_push_tail(program, o);
      break;
    };

    // Booleans
    case BOOL: {
      auto o = allocator->create_boolean(yytext);
      g_queue_push_tail(program, o);
      break;
    }

    // Strings
    case STRING: {
      // `+ 1` removes front parens
      // `- 2` removes back parens
      string preTranslation(yytext + 1, yyleng - 2);
      string translated = translate_escaped_string(preTranslation);
      auto o = allocator->create_string(translated);
      g_queue_push_tail(program, o);
      break;
    }
    case STRING_NESTED: // keep going
      continue;
    case STRING_HEX: {
      string hex_str;
      if (yyleng % 2 != 0) {
        hex_str = string(yytext + 1, yyleng - 1);
        hex_str[hex_str.size() - 1] = '0'; // add 0 to end per PLRM
      } else {
        // even, just remove <arrows>
        hex_str = string(yytext + 1, yyleng - 2);
      }

      string fnl = translate_hex_string(hex_str);
      auto o = allocator->create_string(fnl);
      g_queue_push_tail(program, o);
      break;
    }
    STUB(STRING_B85); // TODO: Implement Base85 string parsing

    // Names
    case NAME: {
      auto o = allocator->create_name(yytext, yyleng);
      g_queue_push_tail(program, o);
      break;
    }
    case NAME_LIT: {
      auto o = allocator->create_name_literal(yytext+1, yyleng-1);
      g_queue_push_tail(program, o);
      break;
    }

    default:
      printf("unknown token type: %d; matched text: %s\n", ret, yytext);
    }
  }

  return 0;
}
