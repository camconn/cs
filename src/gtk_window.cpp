/*
 * gtk_window.cpp - GTK UI Setup
 *
 * © 2020 Cameron Conn
 */


#include <cstdlib>
#include <unistd.h>

#include <cairo.h>
#include <gdk/gdk.h>
#include <gtk/gtk.h>

#include "graphics.hpp"
#include "gtk_window.hpp"


static int scale_factor = 0;
GObject* display;


static void close_window() {
  display = nullptr;

  gtk_main_quit();
}

static void scale_image_by(int direction) {
  if (display == nullptr || cr_surface == nullptr) return;

  // Don't let the user scale too much
  // TODO: Be more consistent with this.
  if ((scale_factor + direction < -8) ||
      (scale_factor + direction > 16)) {
    return;
  }

  scale_factor += direction;

  GdkPixbuf* old = gtk_image_get_pixbuf(GTK_IMAGE(display));
  if (old == nullptr) return;

  GdkPixbuf* orig = gdk_pixbuf_get_from_surface(cr_surface,
                                                0,
                                                0,
                                                pg_width,
                                                pg_height);
  if (orig == nullptr) return;
  GdkPixbuf* scaled;
  scaled = gdk_pixbuf_scale_simple(orig,
                                   pg_width  * (1 + scale_factor*0.10),
                                   pg_height * (1 + scale_factor*0.10),
                                   GDK_INTERP_BILINEAR);
  if (scaled == nullptr)
    g_object_unref(orig);

  gtk_image_set_from_pixbuf(GTK_IMAGE(display), scaled);
  g_object_unref(old);
  g_object_unref(orig);
}

static void zoom_in(GtkWidget* widget, gpointer data) {
  scale_image_by(1);
}

static void zoom_out(GtkWidget* widget, gpointer data) {
  scale_image_by(-1);
}

static void save_image(GtkWidget* widget, gpointer data) {
  GtkWidget* window;
  GtkWidget* dialog;
  GtkFileChooser *chooser;
  GtkFileChooserAction action = GTK_FILE_CHOOSER_ACTION_SAVE;
  GtkFileFilter *filter;
  gint res;

  window = gtk_widget_get_toplevel(widget);

  filter = gtk_file_filter_new();
  gtk_file_filter_add_pattern(filter, "*.png");
  gtk_file_filter_set_name(filter, "PNG Files");

  dialog = gtk_file_chooser_dialog_new("Save File",
                                       GTK_WINDOW(window),
                                       action,
                                       "Cancel",
                                       GTK_RESPONSE_CANCEL,
                                       "Save",
                                       GTK_RESPONSE_ACCEPT,
                                       NULL);
  chooser = GTK_FILE_CHOOSER(dialog);
  gtk_file_chooser_add_filter(chooser, filter);
  gtk_file_chooser_set_current_name(chooser, "render.png");
  gtk_file_chooser_set_do_overwrite_confirmation(chooser, TRUE);

  res = gtk_dialog_run(GTK_DIALOG(dialog));
  if (res == GTK_RESPONSE_ACCEPT) {
      char *filename;

      filename = gtk_file_chooser_get_filename(chooser);
      auto status = cairo_surface_write_to_png(cr_surface, filename);
      if (status == CAIRO_STATUS_SUCCESS) {
        printf("Successfully save to %s\n", filename);
      } else {
        fprintf(stderr, "An error occured while attempting to save image\n");
      }
      g_free(filename);
  }

  gtk_widget_destroy(dialog);
}

void activate(cairo_surface_t* image) {
  GtkBuilder* builder;
  GObject* window;
  display = nullptr;
  GObject* button;
  GError *error = nullptr;

  gtk_init(nullptr, nullptr);

  builder = gtk_builder_new();
  if (gtk_builder_add_from_file(builder, "ui/display.ui", &error) == 0) {
    g_printerr("Error loading file: %s\n", error->message);
    g_clear_error(&error);
    return;
  }

  window = gtk_builder_get_object(builder, "window");
  g_signal_connect_swapped(window, "destroy", G_CALLBACK(close_window), NULL);

  display = gtk_builder_get_object(builder, "display");
  if (cr_surface != nullptr) {
    GdkPixbuf* buf = gdk_pixbuf_get_from_surface(cr_surface,
                                                 0,
                                                 0,
                                                 pg_width,
                                                 pg_height);
    gtk_image_set_from_pixbuf(GTK_IMAGE(display), buf);
  }

  button = gtk_builder_get_object(builder, "quit");
  g_signal_connect_swapped(GTK_WIDGET(button), "clicked", G_CALLBACK(gtk_window_close), window);

  button = gtk_builder_get_object(builder, "zoom-in");
  g_signal_connect(GTK_WIDGET(button), "clicked", G_CALLBACK(zoom_in), nullptr);
  button = gtk_builder_get_object(builder, "zoom-out");
  g_signal_connect(GTK_WIDGET(button), "clicked", G_CALLBACK(zoom_out), nullptr);
  button = gtk_builder_get_object(builder, "save");
  g_signal_connect(GTK_WIDGET(button), "clicked", G_CALLBACK(save_image), nullptr);

  gtk_main();
}

int create_window(cairo_surface_t* image) {
  // TODO: Figure out how to do this in another thread
  activate(image);

  return true;
}
