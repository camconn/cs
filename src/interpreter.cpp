/*
 * interpreter.cpp - PostScript interpreter.
 *
 * © 2020 Cameron Conn
 */

#include <cstdio>
#include <cstring>

#include <string>
#include <utility>

#include <glib.h>

#include "common.hpp"
#include "interpreter.hpp"
#include "memory.hpp"
#include "object.hpp"

using std::printf;

/*
 * Execute a program that has been converted to a stream of tokens.
 *
 * Returns 0 on success. Nonzero on failure.
 */
int execute(GQueue *stack, GQueue *program) {

  GQueue *context = g_queue_new(); // GQueue* of (Object*)
  g_queue_push_tail(context, program);

  while (!g_queue_is_empty(context)) {
    GQueue *current = (GQueue*) g_queue_peek_tail(context);
    if (current->length == 0 || g_queue_get_length(current) == 0) {
      g_queue_pop_tail(context);
      g_queue_free(current);
      continue;
    }


#ifdef CS_STRESS_GC
    allocator->collect_garbage(stack, context);
#else
    if (allocator->next_unique_id % 1024 == 0) {
      allocator->collect_garbage(stack, context);
    }
#endif

    auto op = (Object*) g_queue_pop_head(current);

    // TODO: Handle exceptions the correct way according to PostScript
    if (stack->length >= 1 &&
        ((Object*)g_queue_peek_tail(stack))->type == Exception) {
      printf("Got exception from previous instruction in interpreter. "
             "Aborting!\n");
      return 2;
    }

    // Executable Objects
    if (op->attribs[ExecutableBit] == 1) {
      switch(op->type) {
      case Array:
        g_queue_push_tail(stack, op);
        continue;

      case File: // TODO
        printf("Unimplemented: executable files\n");
        continue;

      case String: // TODO
        printf("Unimplemented: executable strings\n");
        continue;

      case Name: {
        // Executable name: attempt to resolve name
        std::string name = *op->name;

        Object* dictObj = allocator->lookup(name);
        if (dictObj != nullptr) {

          // If we just looked up an procedure,
          // execute it next instruction.
          if (dictObj->type == Array && dictObj->attribs[ExecutableBit] == 1) {
            GQueue *newContext = g_queue_new();

            // TODO: Implement tail recursion. PLRM page 50
            auto append = [](gpointer data, gpointer userdata) {
                            auto o = (Object*) data;
                            auto cxt = (GQueue*) userdata;
                            g_queue_push_tail(cxt, o);
                          };
            g_ptr_array_foreach(dictObj->arr->arr, append, newContext);

            // Push onto context
            g_queue_push_tail(context, newContext);
            continue;
          }

          // Push resolved object onto the front of the program stack.
          // We can execute it next loop
          g_queue_push_head(current, dictObj);
        } else {
          fprintf(stderr, "Could not find key: %s\n", name.c_str());
          g_queue_push_head(stack, allocator->create_exception());
        }
        continue;
      }

      case Native: {
        PSProcedure proc = (PSProcedure) op->data;
        (*proc)(stack);
        continue;
      }

      case Null:
        // no-op, PLRM page 51
        continue;

      default:
        break;
      }
    }

    // Literals
    switch(op->type) {
    // Things that just go onto the stack.
    // Refer to 3.5.5 of the PLRM, page 50
    case Array:
    case Boolean:
    case Dictionary:
    case File:
    case FontID:
    case Integer:
    case Name: // Name literal, just push onto stack
    case Real:
    case Save:
    case String:
      goto push;

    case Mark:
      if (op->attribs[ExecutableBit] == 1) {
        // Create a procedure in-place

        GPtrArray *arr; // Object*

        //Object* i = program->front();
        auto i = (Object *)g_queue_peek_head(current);
        while (current->length > 0) {
          if (i->type == Mark_End &&
              i->attribs[ExecutableBit] == 1) {
            g_queue_pop_head(current); // discard mark
            break;
          }
            
          g_ptr_array_add(arr, i);

          g_queue_pop_head(current);
          i = (Object *)g_queue_peek_head(current);
        }

        PSArray* ps = allocator->create_gc_array(arr);
        auto o = allocator->create_array(ps);
        o->attribs[ExecutableBit] = 1;
        g_queue_push_tail(stack, o);

        // We're done now. Don't continue
        break;
      }

      // If the mark isn't executable, go ahead and push it onto the stack
      // NB: This "push" is shared with the other object types above.
    push:
      g_queue_push_tail(stack, op);
      break;

    case Mark_End: {
      const size_t num = stack->length;
      size_t prev_mark = num;

      for (int i = num - 1; i >= 0; i--) {
        if (((Object*)g_queue_peek_nth(stack, i))->type == Mark) {
          prev_mark = i;
          break;
        }
      }

      if (prev_mark == num) {
        fprintf(stderr, "could not find previous mark\n");
        g_queue_push_tail(stack, allocator->create_exception());
        break;
      }

      GPtrArray *arr; // Object*
      for (int i = prev_mark + 1; i < num; i++) {
        g_ptr_array_add(arr, g_queue_peek_nth(stack, i));
      }

      PSArray* ps = allocator->create_gc_array(arr);
      auto o = allocator->create_array(ps);

      while (stack->length > prev_mark)
        g_queue_pop_tail(stack);
      g_queue_push_tail(stack, o);
      break;
    }

    case Null:
      // for now, treat as a no-op
      // TODO: Is it defined what we should do for literal Nulls?
      break;

    default:
      printf("Unsupported ObjectType: %d\n", op->type);
      return 1;
    }
  }

  if (stack->length != 0 &&
      ((Object*)g_queue_peek_tail(stack))->type == Exception) { 
    fprintf(stderr, "Got exception from last instruction in interpreter.\n");
    return 3;
  }

  return 0;
}
