/*
 * op_graphics.hpp - PostScript graphics operators.
 *
 * © 2020 Cameron Conn
 */

#include <cstdio>

#include <string>

#include <cairo.h>

#include "font.hpp"
#include "interpreter.hpp"
#include "memory.hpp"
#include "op.hpp"
#include "object.hpp"


NATIVE(findfont) {
  REQUIRE_ARGS(stack, 1);

  auto o = (Object*) g_queue_pop_tail(stack);

  if (o->type != Name || o->attribs[ExecutableBit] != 0) {
    fprintf(stderr, "Did not receive literal name as operand\n");
    g_queue_push_tail(stack, allocator->create_exception());
    return;
  }

  int64_t id = find_font(o->name->c_str());
  auto fnt = allocator->create_font(id);

  g_queue_push_tail(stack, fnt);
}


NATIVE(setfont) {
  REQUIRE_ARGS(stack, 1);

  auto f = (Object*) g_queue_pop_tail(stack);

  if (f->type != FontID) {
    fprintf(stderr, "Need to receive a FontID for setfont");
    g_queue_push_tail(stack, allocator->create_exception());
    return;
  }

  use_font(f->i);
}

NATIVE(scalefont) {
  REQUIRE_ARGS(stack, 2);

  auto scale_arg = (Object*) g_queue_pop_tail(stack);
  auto font_arg = (Object*) g_queue_pop_tail(stack);

  if (!is_num(scale_arg) || font_arg->type != FontID) {
    g_queue_push_tail(stack, allocator->create_exception());
    return;
  }

  double scale = get_double(scale_arg);
  if (scale < 0) {
    fprintf(stderr, "scalefont: scale argument cannot be < 0: %lf\n", scale);
    g_queue_push_tail(stack, allocator->create_exception());
    return;
  }

  int64_t id = scale_font(font_arg->i, scale);

  auto out = allocator->create_font(id);
  g_queue_push_tail(stack, out);
}

