/*
 * dict.hpp - PostScript dictionary functions.
 *
 * © 2020 Cameron Conn
 */


#include <map>
#include <string>
#include <utility>

#include "dict.hpp"
#include "interpreter.hpp"
#include "memory.hpp"
#include "object.hpp"
#include "op.hpp"


using std::make_pair;

#define PAIR(name) make_pair(#name, &native_ ## name)
// `PAIR(opname)` expands to:
//   std::make_pair(opname, &native_opname)

std::pair<const char*,PSProcedure> sys_defaults[] = {
  // Stack
  PAIR(clear),
  PAIR(cleartomark),
  PAIR(copy),
  PAIR(count),
  PAIR(counttomark),
  PAIR(dup),
  PAIR(exch),
  PAIR(index),
  PAIR(mark),
  PAIR(pop),
  PAIR(roll),
  make_pair("stack",      &native_print_stack),
  make_pair("=",          &native_print),

  // Bool/Logic Operators
  PAIR(and),
  PAIR(eq),
  PAIR(le),
  PAIR(lt),
  PAIR(ne),
  PAIR(or),
  PAIR(ge),
  PAIR(gt),
  PAIR(xor),

  // Dictionaries
  PAIR(begin),
  PAIR(def),
  PAIR(dict),
  PAIR(end),

  // Fonts
  PAIR(findfont),
  PAIR(setfont),
  PAIR(scalefont),

  // Math
  PAIR(add),
  PAIR(sub),
  PAIR(mul),
  PAIR(div),
  PAIR(idiv),
  PAIR(mod),
  PAIR(abs),
  PAIR(neg),
  make_pair("ceiling",    &native_ceil),
  PAIR(floor),
  PAIR(round),
  make_pair("truncate",   &native_trunc),
  PAIR(sqrt),
  PAIR(exp),
  PAIR(sin),
  PAIR(cos),
  PAIR(atan),
  PAIR(ln),
  PAIR(log),
  PAIR(rand),
  PAIR(rrand),
  PAIR(srand),

  // Graphics
  PAIR(lineto),
  PAIR(moveto),
  PAIR(rlineto),
  PAIR(rmoveto),
  PAIR(arc),
  PAIR(curveto),
  PAIR(newpath),
  PAIR(closepath),
  PAIR(fill),
  PAIR(stroke),
  PAIR(setgray),
  PAIR(currentgray),
  PAIR(setlinewidth),
  PAIR(currentlinewidth),
  PAIR(show),
  PAIR(showpage),
};

PSDict* create_user_dict() {
  PSDict* d = new PSDict;
  d->dict = new Dict;
  return d;
}

PSDict* create_global_dict() {
  PSDict* d = new PSDict;
  d->dict = new Dict;
  return d;
}

PSDict* create_system_dict() {
  PSDict* ps_d = new PSDict;
  Dict* d = new Dict;
  ps_d->dict = d;


  for (auto pair : sys_defaults) {
    std::string key(pair.first);
    Object* proc = allocator->create_procedure(pair.second);
    (*d)[key] = proc;
  }

  // PLRM page 630
  auto n = allocator->create_null();
  (*d)["null"] = n;
  return ps_d;
}
