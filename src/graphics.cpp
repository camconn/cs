/*
 * graphics.cpp - Basic Graphics Setup
 *
 * © 2020 Cameron Conn
 */

#include <cstdio>

#include <cairo.h>
#include <gtk/gtk.h>

#include "font.hpp"
#include "graphics.hpp"
#include "matrix.hpp"

cairo_t* cr_context;
cairo_surface_t* cr_surface;

/**
 * Setup Cairo graphics and load default configuration from FontConfig.
 */
bool setup_graphics() {
  cr_surface = cairo_image_surface_create(CAIRO_FORMAT_ARGB32,
                                          pg_width,
                                          pg_height);
  if (cr_surface == nullptr)
    return false;

  cr_context = cairo_create(cr_surface);
  if (cr_context == nullptr)
    return false;

  // Now, we set up the default options, such as:
  // Colors
  cairo_set_source_rgb(cr_context, 0.0, 0.0, 0.0);
  cairo_set_line_width(cr_context, 1);

  // Cairo->Postscript Transforms
  const cairo_matrix_t ps = cairo_to_ps_matrix();
  cairo_transform(cr_context, &ps);

  // Fonts
  if (!setup_fonts()) {
    fprintf(stderr, "Failed setup_fonts\n");
    return false;
  }

  return true;
}

void cleanup_graphics() {
  cleanup_fonts();

  if (cr_surface != nullptr) {
    cairo_surface_destroy(cr_surface);
  }
  if (cr_context != nullptr) {
    cairo_destroy(cr_context);
  }
}

