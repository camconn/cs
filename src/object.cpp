/*
 * object.cpp - PostScript objects.
 *
 * © 2020 Cameron Conn
 */

#include <cassert>
#include <cinttypes>
#include <cmath>
#include <cstdint>
#include <cstdio>
#include <cstring>

#include <iostream> // I'm gonna regret this.
#include <limits>
#include <string>


#include "memory.hpp"
#include "object.hpp"
#include "util.hpp"


using std::string;

//========================================
//========= Operator Overloads ===========
//========================================

Object* Object::operator+ (const Object &other) {
  // Here we use `can_add` instead of `can_arith_relate` because we
  // also support the `add` operator for Strings.
  if (!can_add(*this, other)) {
    return allocator->create_exception();
  }

  switch(this->type) {
  case Integer:
    if (other.type == Integer) {
      int64_t a = this->i;
      int64_t b = other.i;
      return allocator->create_int(a + b);
    } else {
      goto mixed;
    }
    break;

  case Real:
    if (other.type == Real) {
      double a = this->d;
      double b = other.d;
      return allocator->create_real(a + b);
    } else {
    mixed:
      // A combination of a Real and an Integer gives us a Real.
      double a = get_double(*this);
      double b = get_double(other);

      return allocator->create_real(a + b);
    }
    break;
    
  case String: {
    // Concatenation of Strings
    if (other.type != String) { // redundant check, but just be safe
      goto fail;
    }

    std::string* mine = this->str->str;
    std::string* others = other.str->str;

    std::string final_string = *mine + *others;

    auto ret = allocator->create_string(final_string);
    return ret;
  }

  default: {
    fail:
        assert(false); // This should not be possible
        break;
    }
  }
}

Object* Object::operator- (const Object &other) {
  if (!can_arith_relate(*this, other)) {
    return allocator->create_exception();
  }

  switch(this->type) {
  case Integer:
    if (other.type == Integer) {
      int64_t a = this->i;
      int64_t b = other.i;
      return allocator->create_int(a - b);
    } else {
      goto mixed;
    }

  case Real:
    if (other.type == Real) {
      double a = this->d;
      double b = other.d;
      return allocator->create_real(a - b);
    } else {
    mixed:
      double a = get_double(*this);
      double b = get_double(other);

      return allocator->create_real(a - b);
    }
  default:
  fail:
    assert(false); // This should not be possible
    break;
  }
}

Object* Object::operator* (const Object &other) {
  if (!can_arith_relate(*this, other)) {
    return allocator->create_exception();
  }

  switch(this->type) {
  case Integer:
    if (other.type == Integer) {
      int64_t a = this->i;
      int64_t b = other.i;
      return allocator->create_int(a * b);
    } else {
      goto mixed;
    }

  case Real:
    if (other.type == Real) {
      double a = this->d;
      double b = other.d;
      return allocator->create_real(a * b);
    } else {
    mixed:
      // A combination of a Real and an Integer gives us a Real.
      double a = get_double(*this);
      double b = get_double(other);

      return allocator->create_real(a * b);
    }
  default:
  fail:
    assert(false); // This should not be possible
    break;
  }
}

Object* Object::operator/ (const Object &other) {
  if (!can_arith_relate(*this, other)) {
    return allocator->create_exception();
  }

  switch(this->type) {
  case Integer:
    if (other.type == Integer) {
      double a = (double) this->i;
      double b = (double) other.i;
      if (b == 0) {
        return allocator->create_exception();
      } else {
        return allocator->create_real(a / b);
      }
    } else {
      goto mixed;
    }

  case Real:
    if (other.type == Real) {
      double a = this->d;
      double b = other.d;
      if (b == 0) {
        return allocator->create_exception();
      } else {
        return allocator->create_real(a / b);
      }
    } else {
    mixed:
      double num = get_double(*this);
      double denom = get_double(other);

      if (denom == 0)
        return allocator->create_exception();

      return allocator->create_real(num / denom);
    }
  default:
  fail:
    assert(false); // This should not be possible
    break;
  }
}

Object* Object::operator% (const Object &other) {
  if (this->type != Integer || other.type != Integer) {
    return allocator->create_exception();
  }

  int64_t a = this->i;
  int64_t b = other.i;

  if (b == 0) {
    // modulo zero is undefined
    return allocator->create_exception();
  }

  return allocator->create_int(a % b);
}

const char* type_to_str(Object o) {
  switch (o.type) {
  case Null: return "Null";
  case Array: return "Array";
  case Boolean: return "Boolean";
  case Dictionary: return "Dictionary";
  case Exception: return "Exception";
  case File: return "File";
  case FontID: return "FontID";
  case Integer: return "Integer";
  case Mark: return "Mark";
  case Mark_End: return "Mark_End";
  case Name:
    if (o.attribs[ExecutableBit] == 1) {
      return "Name";
    } else {
      return "NameLiteral";
    }
  case Native: return "Native";
  case Real: return "Real";
  case Save: return "Save";
  case String: return "String";
  default: return "unknown";
  }
}

/**
 * Compare if two Objects are equal. This *DOES NOT* match
 * how things are defined in the PostScript standard.
 *
 * operator== is defined even between Objects of different types.
 */
bool operator== (const Object& lhs, const Object& rhs) {
  if (is_num(lhs) && is_num(rhs)) {
    if (lhs.type == Integer && rhs.type == Integer) {
      return lhs.i == rhs.i;
    }

    double me = get_double(lhs);
    double them = get_double(rhs);

    // Adapted from TAOCP and from the following:
    // https://stackoverflow.com/a/253874
    double smaller = fmin(me, them);
    double larger = fmax(me, them);

    return (larger - smaller) <= std::numeric_limits<double>::epsilon() * 15;
  }

  // We have a mix of (String, Name) types
  if ((lhs.type == String && rhs.type == Name) ||
      (lhs.type == Name && rhs.type == String)) {

    string a = (lhs.type == String) ? *lhs.str->str : *lhs.name;
    string b = (rhs.type == String) ? *rhs.str->str : *rhs.name;
    return a == b;
  }

  if (lhs.type != rhs.type) return false;
  // After this we can assume both objects have the same PostScript type

  switch (lhs.type) {
  case Array: {
    // Check for pointers to the same array.
    if (lhs.arr == rhs.arr
        || lhs.arr->arr == rhs.arr->arr)
      return true;

    GPtrArray *larr = lhs.arr->arr;
    GPtrArray *rarr = rhs.arr->arr;
    if (larr->len != rarr->len)
      return false;

    // They have the same size, linear scan now.
    for (int i = 0; i < larr->len; i++) {
      auto left = (Object*) g_ptr_array_index(larr, i);
      auto right = (Object*) g_ptr_array_index(rarr, i);

      if (*left != *right)
        return false;
    }

    return true;
  }

  case Boolean:
    return lhs.b == rhs.b;

  case Dictionary:
    printf("TODO: Implement == Dictionary\n");
    assert(false);
  case File:
    printf("TODO: Implement == File\n");
    assert(false);
  case FontID:
    printf("TODO: Implement == FontID\n");
    assert(false);

  case Exception:
    return true;
    break;

  case Mark:
  case Mark_End:
    return true;

  case Name: {
    string me, them;
    me = *lhs.name;
    them = *rhs.name;
    return me == them;
  }

  case String: {
    auto left = lhs.str->str;
    auto right = rhs.str->str;

    return *left == *right;
  }

  case Native:
    return lhs.data == rhs.data;

  case Null:
    return lhs.data == rhs.data;

  default:
    printf("unhandled ObjectType in ==\n");
    break;
  }
  return false;
}

bool operator!= (const Object& lhs, const Object& rhs) {
  return !(lhs == rhs);
}


std::ostream& operator<< (std::ostream&, Object*);
std::ostream& operator<< (std::ostream&, const Object&);

std::ostream& operator<< (std::ostream& os, Object* o) {
  os << *o;
  return os;
}

std::ostream& operator<< (std::ostream& os, const Object& o) {
  os << "<type: " << type_to_str(o) << " data: ";
  switch(o.type) {
  case Array: {
    os << '{';

    auto print_elem = [](gpointer d, gpointer ud) {
                        std::ostream& os = *(std::ostream*)ud;
                        auto i = (Object*) d;
                        os << *i << ',';
                      };
    g_ptr_array_foreach(o.arr->arr, print_elem, &os);

    os << '}';
  }

    break;

  case Boolean:
    if (o.b)
      os << "true";
    else
      os << "false";

    break;
    
  case Name:
    os << *o.name;
    break;

  case String:
    os << *o.str->str;
    break;

  case Integer:
    os << o.i;
    break;
  case Real:
    os << o.d;
    break;

  case FontID:
    os << "fontid: " << o.i;
    break;

  default:
    if (o.data == nullptr) {
      os << "(unimplemented/nullptr)";
    } else {
      os << "(unimplemented/omitted)";
    }
    break;
  }
  os << '>';
  return os;
}

//========================================
//========= Utility Functions ============
//========================================

// Print an object o followed by a newline.
void print_object_newline(const Object& o) {
  print_object(o);
  printf("\n");
}

// Print an object o.
void print_object(const Object& o) {
  switch (o.type) {
  case Array: {
    // TODO: Check if this is an array or a procedure
    // by using the `executable` attribute.

    // A procedure is an executable array.
    bool is_procedure = o.attribs[ExecutableBit] == 1;
    if (is_procedure) {
      printf("{");
    } else {
      printf("[");
    }

    auto mem = (GPtrArray*) o.arr->arr;

    const size_t sz = mem->len;
    for (int i = 0; i < sz; i++) {
      if (i > 0 && sz >= 2)
        putchar(' ');

      //print_object(*(mem[i]));
      print_object(*(Object*)g_ptr_array_index(mem, i));
    }

    if (is_procedure) {
      printf("}");
    } else {
      printf("]");
    }
    break;
  }
  case Boolean:
    if (o.b) {
      printf("true");
    } else {
      printf("false");
    }
    break;

  case Mark:
    if (o.attribs[ExecutableBit] == 1) {
      putchar('{');
    } else {
      putchar('[');
    }
    break;
  case Mark_End:
    if (o.attribs[ExecutableBit] == 1) {
      putchar('}');
    } else {
      putchar(']');
    }
    break;

  case Native:
    printf("<Native procedure>");
    break;

  case Integer:
    printf("%ld", o.i);
    break;
  case Real:
    printf("%lf", o.d);
    break;

  case Name:
    // BUG: This needs to support strings with NULL-bytes in them
    printf("%s", o.name->c_str());
    break;

  case String:
    // BUG: This needs to support strings with NULL-bytes in them
    printf("%s", o.str->str->c_str());
    break;

  default:
    printf("o's type is UNSUPPORTED to print");
    break;
  }
}

/**
 * Get the access type for this object.
 */
AccessType get_permissions(Object* o) {
  int bit0 = o->attribs[AccessBit0];
  int bit1 = o->attribs[AccessBit1];
  int total = 2*bit1 + 1*bit0;

  return static_cast<AccessType>(total);
}

/**
 * The the access for this object.
 */
void set_permissions(Object* o, AccessType t) {
  int bits = static_cast<int>(t);
  int bit0 = (bits & 0x01);
  int bit1 = (bits & 0x02) >> 1;

  o->attribs[AccessBit0] = bit0;
  o->attribs[AccessBit1] = bit1;
}

// Can a and b arithmetically relate?
bool can_arith_relate(const Object& a, const Object& b) {
  return (a.type == Integer || a.type == Real) &&
         (b.type == Integer || b.type == Real);
}

// Can we "add" these objects together (e.g. numbers, strings)
bool can_add(const Object &a, const Object &b) {
  return (can_arith_relate(a, b) ||
          (a.type == String && b.type == String));
}

// Is this object a Number?
bool is_num(const Object& o) {
  return o.type == Integer || o.type == Real;
}

// Is this object a Number?
bool is_num(Object* o) {
  return is_num(*o);
}

// Is this object an Integer?
bool is_int(const Object& o) {
  return o.type == Integer;
}

// Is this object an Integer?
bool is_int(Object* o) {
  return is_int(*o);
}

// Is this object a Real?
bool is_real(const Object& o) {
  return o.type == Real;
}

// Is this object a Real?
bool is_real(Object* o) {
  return is_real(*o);
}

// Get a raw double from a Real or Integer object.
double get_double(const Object& o) {
  if (o.type == Real) {
    return o.d;
  } else if (o.type == Integer) {
    return (double) o.i;
  } else {
    assert(false);
  }

  assert(false);
}

double get_double(Object* o) {
  return get_double(*o);
}

// Get a raw integer from an object, with no casting
// from a Real object.
int64_t get_int(const Object& o) {
  if (o.type == Integer) {
    return o.i;
  }

  assert(false);
}

int64_t get_int(Object* o) {
  return get_int(*o);
}

