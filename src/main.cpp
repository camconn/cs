/*
 * main.cpp - main executable for cs
 *
 * © 2020 Cameron Conn
 */

#include <cassert>
#include <cstdio>

#include <cairo.h>

#include "font.hpp"
#include "graphics.hpp"
#include "gtk_window.hpp"
#include "interpreter.hpp"
#include "lexer.hpp"
#include "memory.hpp"
#include "object.hpp"

int main(int argc, char** argv) {
  int lexresult;
  int execution;

  FILE* inp_file = stdin;
  bool using_file = false;
  if (argc >= 2) {
    inp_file = fopen(argv[1], "r");
    if (inp_file == nullptr) {
      fprintf(stderr, "Exiting: Could not open %s\n", argv[1]);
      exit(1);
    }
    using_file = true;
  }

  init_vm();

  Stack program = g_queue_new();
  Stack stack = g_queue_new();
  printf("= BEGIN LEXER ==========================\n");
  lexresult = lexer(program, inp_file);
  if (lexresult != 0) {
    printf("There was an error with lexing: %d\n", lexresult);
    exit(6);
  }

  if (using_file) {
    fclose(inp_file);
  }

  if (!setup_graphics()) {
    printf("setup_graphics could not setup graphics\n");
    exit(7);
  }

  printf("= BEGIN EXECUTION ======================\n");
  execution = execute(stack, program);
  if (execution != 0) {
    printf("Execution returned with error code: %d\n", execution);
    exit(2);
  }

  if (!create_window(cr_surface)) {
    printf("Could not create GTK window to display cairo drawing\n");
    exit(8);
  }

  cleanup_graphics();

  return 0;
}
