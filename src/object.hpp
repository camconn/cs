/*
 * object.hpp - PostScript objects.
 *
 * © 2020 Cameron Conn
 */

#ifndef CS_OBJECT_H
#define CS_OBJECT_H

#include <cstdint>

#include <bitset>
#include <map>
#include <string>

#include <glib.h>

/**
 * An ObjectType denotes what kind of PostScript object
 * and Object is. CS uses this in lieu of C++'s polymorphism
 * to make the code more like C.
 */
enum ObjectType {
  Null,
  Array,
  Boolean,
  Dictionary,
  Exception,
  File,
  FontID,
  Integer,
  Mark,
  /** Represents `}`, `]`, and `>>` */
  Mark_End,
  Name,
  /**
   * A native-compiled procedure that is built-in to CS.
   *
   * This replaces the `Operator` object type specified in the PLRM.
   */
  Native,
  Real,
  Save,
  String,
};

/**
 * The Color enum is used to track state for a tricolor
 * garbage collector.
 */
enum Color {
  /** A reachable node whose children have all been visited. */
  Black,
  /** A reachable node adjacent to a Black node. */
  Gray,
  /** An unvisited node */
  White,
};

struct Object;
/** A shared reference to a PostScript dictionary. */
struct PSDict {
  Color color;
  std::map<std::string,Object*>* dict;
};
/** A shared reference to a PostScript array of any kind. */
struct PSArray {
  Color color;
  GPtrArray *arr; // Object*
};
/** A shared reference to a PostScript string. */
struct PSString {
  Color color;
  std::string* str;
};

/**
 * Each Object uses an inline bit-field for storing
 * commonly-used Object attributes. Here is the documentation for
 * that bit-field.
 *
 * Inline Object Attribute Bit-field
 * =================================
 *  0000 0000
 *        |||
 *        ||+==> Executable Object Indicator (0 implies literal)
 *        || 
 *        ||
 *        ||
 *        ++===> Access Bits (cf PLRM page 37)
 *               11 - Unlimited;    10 - Read Only
 *               01 - Execute-only; 00 - None
 */

const size_t ExecutableBit  = 0;
const size_t AccessBit0     = 1;
const size_t AccessBit1     = 2;

enum AccessType {
  None      = 0,
  Execute   = 1,
  Read      = 2,
  Unlimited = 3,
};


/**
 * An Object is a representation of PostScript object.
 */
struct Object {
  union {
    int64_t       i;            // inline integer value
    double        d;            // inline double-precision float
    bool          b;            // inline boolean
    std::string*  name;         // used for name & font lookups
    void*         data;         // used to check for nullptrs
    PSArray*      arr;
    PSDict*       dict;
    PSString*     str;
  };

  ObjectType      type;
  Color           color;
  std::bitset<4>  attribs;

  Object* operator+ (const Object& other);
  Object* operator- (const Object& other);
  Object* operator* (const Object& other);
  Object* operator/ (const Object& other);
  Object* operator% (const Object& other);
};

bool operator== (const Object& lhs, const Object& rhs);
bool operator!= (const Object& lhs, const Object& rhs);

// Utility functions
bool can_arith_relate(const Object&, const Object&);
bool can_add(const Object&, const Object&);
bool is_num(const Object&);
bool is_num(Object*);
bool is_int(const Object&);
bool is_int(Object*);
bool is_real(const Object&);
bool is_real(Object*);
void print_object(const Object&);
void print_object_newline(const Object&);
AccessType get_permissions(Object*);
void set_permissions(Object*, AccessType);
double get_double(const Object&);
double get_double(Object*);
int64_t get_int(const Object&);
int64_t get_int(Object*);

#endif
