/*
 * lexer_test.cpp - Interface to flex for CS
 *
 * © 2020 Cameron Conn
 */

#include <string>

#include "gtest/gtest.h"

#include "lexer.hpp"
#include "object_test.hpp"

using std::string;

TEST(TranslateEscaped, Whitespace) {
    string a = string("hello\\tworld!");
    string b = string("hello\tworld!");

    string c = string("ohai\\r");
    string d = string("ohai\r");

    string e = string("This should work \\\r\nas expected\n here");
    string f = string("This should work as expected\n here");

    // Test newlines in string literals as defined in
    // PLRM page 75
    string g = string("Newline\n in the middle");
    string h = string("Newline\r\n in the middle");
    string i = string("Newline\r in the middle");
    string j = string("Newline\\n in the middle");

    EXPECT_EQ(translate_escaped_string(a), b);
    EXPECT_EQ(translate_escaped_string(c), d);
    EXPECT_EQ(translate_escaped_string(e), f);

    EXPECT_EQ(translate_escaped_string(h), g);
    EXPECT_EQ(translate_escaped_string(i), g);
    EXPECT_EQ(translate_escaped_string(j), g);
}

TEST(TranslateEscaped, Codes) {
    string a = "\\050\\051";
    string b = "()";
    EXPECT_EQ(translate_escaped_string(a), b);
}

TEST(TranslateHex, Basic) {
    string a = "6F6861693F";
    string b = "ohai?";
    EXPECT_EQ(translate_hex_string(a), b);

    string c = "43614D65526F4E";
    string d = "CaMeRoN";
    EXPECT_EQ(translate_hex_string(c), d);
}
