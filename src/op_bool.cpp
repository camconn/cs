/*
 * op_bool.cpp - Boolean & Logical PostScript operators
 *
 * © 2020 Cameron Conn
 */

#include <string>

#include <glib.h>

#include "memory.hpp"
#include "object.hpp"
#include "op.hpp"


#define BOOLEAN_CMP(name, op)                                             \
NATIVE(name) {                                                            \
  REQUIRE_ARGS(stack, 2);                                                 \
                                                                          \
  auto b = (Object*) g_queue_pop_tail(stack);                             \
  auto a = (Object*) g_queue_pop_tail(stack);                             \
                                                                          \
  if (is_num(a) && is_num(b)) {                                           \
    if (a->type == Integer && b->type == Integer) {                       \
      int64_t left = get_int(a);                                          \
      int64_t right = get_int(b);                                         \
      g_queue_push_tail(stack, allocator->create_boolean(left op right)); \
    } else {                                                              \
      double left = get_double(a);                                        \
      double right = get_double(b);                                       \
      g_queue_push_tail(stack, allocator->create_boolean(left op right)); \
    }                                                                     \
  } else if (a->type == String && b->type == String) {                    \
    if (a->str == b->str) {                                               \
      g_queue_push_tail(stack, allocator->create_boolean(string("a") op string("a")));    \
      return;                                                             \
    }                                                                     \
    bool result = *a->str->str op *b->str->str;                           \
    g_queue_push_tail(stack, allocator->create_boolean(result));          \
  } else {                                                                \
    fprintf(stderr, "Arguments must be a number or a string.");           \
    g_queue_push_tail(stack, allocator->create_exception());              \
  }                                                                       \
}

#define BOOLEAN_OP(name, op, op_bool)                                     \
NATIVE(name) {                                                            \
  REQUIRE_ARGS(stack, 2);                                                 \
  auto b = (Object*) g_queue_pop_tail(stack);                             \
  auto a = (Object*) g_queue_pop_tail(stack);                             \
  if (a->type == Integer && b->type == Integer ) {                        \
    g_queue_push_tail(stack, allocator->create_int(a->b op b->b));        \
  } else if (a->type == Boolean && b->type == Boolean) {                  \
    g_queue_push_tail(stack, allocator->create_boolean(a->b op_bool b->b)); \
  } else {                                                                \
    g_queue_push_tail(stack, allocator->create_exception());              \
  }                                                                       \
}

BOOLEAN_CMP(lt, <);
BOOLEAN_CMP(le, <=);
BOOLEAN_CMP(gt, >);
BOOLEAN_CMP(ge, >=);

BOOLEAN_OP(and, &, &&);
BOOLEAN_OP(or, |, ||);
BOOLEAN_OP(xor, ^, ^);

NATIVE(eq) {
  REQUIRE_ARGS(stack, 2);
  Object *b = (Object*) g_queue_pop_tail(stack);
  Object *a = (Object*) g_queue_pop_tail(stack);

  bool result;
  // TODO: Check for objects whose behavior is different from
  // my built-in '==' operator
  if ((a->type == Array || b->type == Array) ||
      (a->type == Dictionary || b->type == Dictionary) ||
      (a->type == File || b->type == File) ||
      (a->type == Save || b->type == Save)) {
    if (a->type == Array && b->type == Array) {
      result = a->arr == b->arr;
    } else if (a->type == Dictionary && b->type == Dictionary) {
      result = a->dict == b->dict;
    } else if (a->type == File && b->type == File) {
      fprintf(stderr, "TODO: Implement `File File eq`");
      result = false;
    } else if (a->type == Save && b->type == Save) {
      fprintf(stderr, "TODO: Implement `Save Save eq`");
      result = false;
    } else {
      result = false;
    }
  } else {
    result = (*a) == (*b);
  }

  auto o = allocator->create_boolean(result);
  g_queue_push_tail(stack, o);
}

NATIVE(ne) {
  REQUIRE_ARGS(stack, 2);

  // Just use PS `eq` and invert result.
  native_eq(stack);
  auto o = (Object*) g_queue_peek_tail(stack);
  o->b = !o->b;
}

