/*
 * memory.cpp - Garbage Collector and Memory Allocators
 *
 * © 2020 Cameron Conn
 */

#include <cassert>
#include <cinttypes>
#include <cstdlib>

#include <string>

#include <glib.h>

#include "common.hpp"
#include "dict.hpp"
#include "interpreter.hpp"
#include "object.hpp"
#include "memory.hpp"
#include "util.hpp"

VM *allocator;

using std::map;
using std::string;

/** Initialize PostScript "Virtual Machine" memory. */
void init_vm() {
  if (allocator != nullptr) {
    GQueue *a = g_queue_new(), *b = g_queue_new();
    allocator->collect_garbage(a, b);
    g_queue_free(a);
    g_queue_free(b);
  }

  allocator = new VM;

  allocator->random_seed = 0x00000000DEADBEEF;
  srand(allocator->random_seed);

  // Setup dynamic arrays
  allocator->arrays = g_ptr_array_new();
  allocator->dicts = g_ptr_array_new();
  allocator->strings = g_ptr_array_new();
  allocator->objs = g_ptr_array_new();
  allocator->fonts = g_ptr_array_new();
  allocator->dict_stack = g_ptr_array_new();

  allocator->builtin_dicts[SYSTEM_DICT] = create_system_dict();
  allocator->builtin_dicts[GLOBAL_DICT] = create_global_dict();
  allocator->builtin_dicts[USER_DICT] = create_user_dict();

  // TODO: Should we place the built-in dictionaries on the
  // dictionary stack, or are the semantics for this wrong?
  g_ptr_array_add(allocator->dict_stack, allocator->builtin_dicts[SYSTEM_DICT]);
  g_ptr_array_add(allocator->dict_stack, allocator->builtin_dicts[GLOBAL_DICT]);
  g_ptr_array_add(allocator->dict_stack, allocator->builtin_dicts[USER_DICT]);
}

/** A manual call to the garbage collector. */
void VM::collect_garbage(GQueue *stack, GQueue *context) {
  // Set all objects to White
  this->gc_reset();

#ifdef CS_DEBUG_GC
  printf("marking\n");
#endif

  this->gc_mark_internals();
  this->gc_mark(stack);

  auto m = [](gpointer data, gpointer userdata) {
             auto queue = (GQueue*) data; // Object*
             auto vm = (VM*) userdata;
             vm->gc_mark(queue);
           };
  g_queue_foreach(context, m, this);

#ifdef CS_DEBUG_GC
  printf("sweeping\n");
#endif

  this->gc_sweep();
}

/** Create an object that is in the GC. */
Object* VM::create_gc_obj() {
  Object *o = new Object;
  o->color = Gray;

  // Make literals with full access by default
  o->attribs[ExecutableBit] = 0;
  o->attribs[AccessBit0] = 1;
  o->attribs[AccessBit1] = 1;

  g_ptr_array_add(objs, o);
  return o;
}

PSArray* VM::create_gc_array(GPtrArray *v) {
  auto o = new PSArray;
  o->color = Gray;
  o->arr = g_ptr_array_copy(v, nullptr, nullptr);

  g_ptr_array_add(arrays, o);
  return o;
}

PSDict* VM::create_gc_dict(map<string,Object*> m) {
  auto o = new PSDict;
  o->color = Gray;
  o->dict = new map<string,Object*>(m);

  g_ptr_array_add(dicts, o);
  return o;
}

PSString* VM::create_gc_string(const string& str) {
  auto o = new PSString;
  o->color = Gray;
  o->str = new string(str);

  g_ptr_array_add(strings, o);
  return o;
}

//========================================
//========= Object Constructors ==========
//========================================
Object* VM::create_array(PSArray *arr) {
  auto o = create_gc_obj();
  o->type = Array;
  o->data = (void*) arr;
  o->attribs[ExecutableBit] = 0;
  return o;
}

Object* VM::create_dict() {
  map<string,Object*> m;
  auto dict = create_gc_dict(m);
  auto o = create_gc_obj();
  o->type = Dictionary;
  o->dict = dict;
  return o;
}
Object* VM::create_dict(int64_t size) {
  return this->create_dict();
}

Object* VM::create_int(const char *text) {
  auto o = create_gc_obj();

  int64_t data;
  sscanf(text, "%" SCNd64, &data);
  o->type = Integer;
  o->i = data;

  return o;
}

Object* VM::create_int(int64_t value) {
  auto o = create_gc_obj();
  o->type = Integer;
  o->i = value;
  return o;
}

Object* VM::create_real(const char *text) {
  double data = std::stod(text);
  return create_real(data);
}

Object* VM::create_real(double value) {
  auto o = create_gc_obj();
  o->type = Real;
  o->d = value;
  return o;
}

Object* VM::create_boolean(bool value) {
  auto o = create_gc_obj();
  o->type = Boolean;
  o->b = value;
  return o;
}

Object* VM::create_boolean(const char *text) {
  if (str_equals(text, "false")) {
    return create_boolean(false);
  } else if (str_equals(text, "true")) {
    return create_boolean(true);
  } else {
    // ERROR
    assert(false);
  }
}

Object* VM::create_exception() {
  auto o = create_gc_obj();
  o->type = Exception;
  o->data = nullptr;
  return o;
}

Object* VM::create_font(int64_t id) {
  auto o = create_gc_obj();
  o->type = FontID;
  o->i = id;
  g_ptr_array_add(this->fonts, o);
  return o;
}

Object* VM::create_mark() {
  auto o = create_gc_obj();
  o->type = Mark;
  o->data = nullptr;
  return o;
}

Object* VM::create_mark_end() {
  auto o = create_gc_obj();
  o->type = Mark_End;
  o->data = nullptr;
  return o;
}

Object* VM::create_null() {
  auto o = create_gc_obj();
  o->type = Null;
  o->data = nullptr;
  return o;
}

Object* VM::create_procedure(PSProcedure p) {
  auto o = create_gc_obj();
  o->type = Native;
  o->data = (void*) p;
  o->attribs[ExecutableBit] = 1;
  return o;
}


//========================================
//========= String Constructors ==========
//========================================

// Just Strings
Object* VM::create_string(const char *text, size_t len) {
  string str(text, len);
  return create_string(str);
}
Object* VM::create_string(const string& text) {
  PSString* str = create_gc_string(text);
  return create_string(str);
}
Object* VM::create_string(PSString *s) {
  auto o = this->create_gc_obj();
  o->type = String;
  o->str = s;
  o->attribs[ExecutableBit] = 0;
  return o;
}

// Names
Object* VM::create_name(const char *text, size_t len) {
  string str(text, len);
  return create_name(str);
}
Object* VM::create_name(const string& text) {
  string *str = new string(text);
  auto o = create_gc_obj();
  o->type = Name;
  o->name = str;
  o->attribs[ExecutableBit] = 1;
  return o;
}

// Name Literals
Object* VM::create_name_literal(const char *text, size_t len) {
  string str(text, len);
  return create_name_literal(str);
}
Object* VM::create_name_literal(const string& text) {
  auto o = create_name(text);
  o->type = Name;
  o->attribs[ExecutableBit] = 0;
  return o;
}

// Clone an object and have it automatically garbage collected.
Object* VM::clone_object(Object *obj) {
  if (obj == nullptr) {
    return create_null();
  }

  auto cloned = create_gc_obj();
  cloned->type = obj->type;
  cloned->data = obj->data;
  cloned->attribs = obj->attribs;
  return cloned;
}
Object* VM::clone_object(Object obj) {
  return clone_object(&obj);
}

Object* VM::lookup(const string& key) {
  // First, lookup in dictionary stack
  for (int i = 0; i < this->dict_stack->len; i++) {
    auto d = (PSDict*) g_ptr_array_index(this->dict_stack, i);
    auto di = d->dict;

    Dict::iterator found = di->find(key);
    if (found != di->end()) {
      // We found it!
      return found->second;
    }
  }

  // Could find key in user stack. Now we should look in the builtin-stacks.

  // Now lookup in built-in dictionaries
  for (int i = 2; i >= 0; --i) {
    Dict *di = this->builtin_dicts[i]->dict;
    auto found = di->find(key);
    if (found != di->end()) {
      return found->second;
    }
  }

  return nullptr;
}

/**
 * Mark internal PostScript objects so they are not deleted during
 * garbage collection
 */
void VM::gc_mark_internals() {
  GQueue queue = G_QUEUE_INIT;

  for (size_t i = 2; i > 0; --i) {
    Dict *d = this->builtin_dicts[i]->dict;
    for (auto j = d->begin(); j != d->end(); ++j) {
      g_queue_push_tail(&queue, j->second);
    }

    if (queue.length > 0) {
      gc_mark(&queue);
      g_queue_clear(&queue);
    }
  }
  g_queue_clear(&queue);

  // Don't get rid of fonts. They are persistent for the life-cycle of
  // the program.
  auto f = [](gpointer data, gpointer user_data) {
             auto font = (Object*) data;
             auto q = (GQueue*) user_data;
             g_queue_push_tail(q, font);
           };
  g_ptr_array_foreach(fonts, f, &queue);
  gc_mark(&queue);
}

void VM::gc_mark(GQueue *state) {
  if (state->length == 0) return;

  GQueue *queue = g_queue_copy(state);

  while (!g_queue_is_empty(queue)) {
    auto front = (Object*) g_queue_pop_head(queue);
    if (front == nullptr)
      continue;

    switch (front->color) {
    case Black:
      continue;

    case White:
      front->color = Gray;
    case Gray:

      switch (front->type) {
      case String: {
        PSString *s = front->str;
        s->color = Black;
        break;
      }
      case Array: {
        PSArray *a = front->arr;
        if (a->color == Black) break;

        a->color = Gray;

        auto process = [](gpointer d, gpointer ud) {
          auto o = (Object*) d;
          auto q = (GQueue*) ud;
          if (o->color == White) {
            o->color = Gray;
            g_queue_push_tail(q, o);
          }
        };
        g_ptr_array_foreach(a->arr, process, queue);

        a->color = Black;
        break;
      }
      case Dictionary: {
        PSDict *d = front->dict;
        if (d->color == Black) break;

        d->color = Gray;

        for (auto p = d->dict->begin(); p != d->dict->end(); ++p) {
          auto o = p->second;
          if (o->color == White) {
            o->color = Gray;
            g_queue_push_tail(queue, o);
          }
        }

        d->color = Black;
        break;
      }
      default:
        break;
      }

      front->color = Black;
    }
  }

  g_queue_free(queue);
}

/**
 * Reset all nodes so they are pearly white.
 *
 * TODO: This can be parallelized
 */
void VM::gc_reset() {
  // Convenience macro
#define MARK_WHITE(type)          \
  [](gpointer d, gpointer ud) {   \
      ((type*)d)->color = White;  \
    };

  auto f = MARK_WHITE(PSArray);
  g_ptr_array_foreach(arrays, f, nullptr);

  auto g = MARK_WHITE(PSString);
  g_ptr_array_foreach(strings, g, nullptr);

  auto h = MARK_WHITE(PSDict);
  g_ptr_array_foreach(dicts, h, nullptr);

  auto i = MARK_WHITE(Object);
  g_ptr_array_foreach(objs, i, nullptr);
  g_ptr_array_foreach(fonts, i, nullptr);
}

// Internal free() used by the garbage collector
void gc_free_object(Object *o) {
  switch (o->type) {
  case Array:
  case Boolean:
  case Dictionary:
  case Exception:
  case File:
  case FontID:
  case Integer:
  case Mark:
  case Mark_End:
  case Native:
  case Null:
  case Real:
  case Save:
  case String:
    // Do nothing
    break;

  case Name:
    delete o->name;
    break;
  }

  delete o;
}

void VM::gc_sweep() {
  for (int i = 0; i < objs->len; i++) {
    auto o = (Object*) g_ptr_array_index(objs, i);
    if (o->color == White && o->type != Native) {
      g_ptr_array_remove_index_fast(objs, i);
      gc_free_object(o);
#ifdef CS_DEBUG_GC
      fprintf(stderr, "deleted object\n");
#endif
      i--;
    }
  }

  for (int i = 0; i < arrays->len; i++) {
    auto arr = (PSArray*) g_ptr_array_index(arrays, i);
    if (arr->color == White) {
      g_ptr_array_remove_index_fast(arrays, i);
      g_ptr_array_free(arr->arr, TRUE);
      delete arr;
#ifdef CS_DEBUG_GC
      fprintf(stderr, "deleted array\n");
#endif
      i--;
    }
  }

  for (int i = 0; i < strings->len; i++) {
    auto str = (PSString*) g_ptr_array_index(strings, i);
    if (str->color == White) {
      g_ptr_array_remove_index_fast(strings, i);
      delete str->str;
      delete str;
#ifdef CS_DEBUG_GC
      fprintf(stderr, "deleted string\n");
#endif
      i--;
    }
  }

  // TODO: Implement permissions for dictionaries
  // so we don't accidentally delete user, global, or
  // system-level dictionaries.
}
