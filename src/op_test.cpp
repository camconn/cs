/*
 * op_test.cpp - Test native stack functions
 *
 * © 2020 Cameron Conn
 */

#include <cmath>
#include <cstdint>

#include "gtest/gtest.h"

#include <glib.h>


#include "object.hpp"
#include "object_test.hpp"
#include "op.hpp"
#include "memory.hpp"

#define a_ allocator
#define add(stk, name, val) g_queue_push_tail(stk, a_->create_ ## name (val));

TEST(OpCopy, Integer) {
  init_vm();

  Stack orig = g_queue_new();
  Stack s = g_queue_new();
  Stack expected = g_queue_new();

  add(orig, int, 1);
  add(orig, real, 5.3);
  add(orig, string, "hello, world!");

  *s = *orig;

  for (int i = 0; i < 2; i++) {
    for (int j = 0; j < orig->length; j++) {
      g_queue_push_tail(expected, g_queue_peek_nth(orig, j));
    }
  }
  g_queue_push_tail(s, a_->create_int(s->length));
  
  native_copy(s);

  ASSERT_EQ(s->length, expected->length);
  for (int i = 0; i < s->length; i++) {
    ASSERT_EQ(*((Object*)g_queue_peek_nth(s, i)),
              *((Object*)g_queue_peek_nth(expected, i)));
  }
}

TEST(OpCopy, Nothing) {
  init_vm();

  Stack orig = g_queue_new();
  Stack s = g_queue_new();
  Stack expected = g_queue_new();

  add(orig, int, 1);
  add(orig, real, 5.3);
  add(orig, string, "hello, world!");

  s = g_queue_copy(orig);
  expected = g_queue_copy(orig);
  g_queue_push_tail(s, allocator->create_int((int64_t)0));

  native_copy(s);

  ASSERT_EQ(s->length, expected->length);

  g_queue_free(s);
  g_queue_free(orig);
  g_queue_free(expected);
}


TEST(OpCopy, String) {
  init_vm();

  Stack orig = g_queue_new();
  Stack expected = g_queue_new();

  add(orig, string, "hey, arnold");
  add(orig, string, "");

  add(expected, string, "hey, arnold");
  add(expected, string, "hey, arnold");

  native_copy(orig);

  ASSERT_EQ(orig->length, expected->length);
  ASSERT_EQ(*((Object*)g_queue_peek_tail(orig))->str->str,
            *((Object*)g_queue_peek_tail(expected))->str->str);
  ASSERT_EQ(*((Object*)g_queue_peek_tail(orig)),
            *((Object*)g_queue_peek_tail(expected)));
  ASSERT_EQ(*((Object*)g_queue_peek_head(orig)),
            *((Object*)g_queue_peek_head(expected)));

  g_queue_free(orig);
  g_queue_free(expected);
}

TEST(OpCopy, Array) {
  init_vm();

  GPtrArray *objs = g_ptr_array_new(); // Object*
  g_ptr_array_add(objs, a_->create_int(5));
  g_ptr_array_add(objs, a_->create_int(6));
  g_ptr_array_add(objs, a_->create_int(7));
  g_ptr_array_add(objs, a_->create_int(8));
  g_ptr_array_add(objs, a_->create_boolean(true));
  g_ptr_array_add(objs, a_->create_boolean(false));

  GPtrArray *empty = g_ptr_array_new(); // Object*

  Stack orig = g_queue_new();
  Stack expected = g_queue_new();


  PSArray *arr = a_->create_gc_array(objs);
  PSArray *empt = a_->create_gc_array(empty);

  g_queue_push_tail(orig, a_->create_array(arr));
  g_queue_push_tail(orig, a_->create_array(empt));

  g_queue_push_tail(expected, a_->create_array(arr));
  g_queue_push_tail(expected, a_->create_array(arr));

  native_copy(orig);

  ASSERT_EQ(*((Object*)g_queue_peek_tail(orig)),
            *((Object*)g_queue_peek_tail(expected)));
  ASSERT_EQ(*((Object*)g_queue_peek_head(orig)),
            *((Object*)g_queue_peek_tail(orig)));

  g_queue_free(orig);
  g_queue_free(expected);

  g_ptr_array_free(objs, TRUE);
  g_ptr_array_free(empty, TRUE);
}

TEST(OpIndex, Typical) {
  init_vm();

  Stack objs = g_queue_new();
  g_queue_push_tail(objs, a_->create_int(5));
  g_queue_push_tail(objs, a_->create_int(6));
  g_queue_push_tail(objs, a_->create_int(7));
  g_queue_push_tail(objs, a_->create_int(8));

  Stack test0 = g_queue_copy(objs),
    test1 = g_queue_copy(objs),
    test2 = g_queue_copy(objs),
    test3 = g_queue_copy(objs);

  g_queue_push_tail(test0, a_->create_int(3));
  g_queue_push_tail(test1, a_->create_int(2));
  g_queue_push_tail(test2, a_->create_int(1));
  g_queue_push_tail(test3, a_->create_int((int64_t)0));

  native_index(test0);
  native_index(test1);
  native_index(test2);
  native_index(test3);


  ASSERT_EQ(((Object*)g_queue_peek_tail(test0))->i, 5);
  ASSERT_EQ(((Object*)g_queue_peek_tail(test1))->i, 6);
  ASSERT_EQ(((Object*)g_queue_peek_tail(test2))->i, 7);
  ASSERT_EQ(((Object*)g_queue_peek_tail(test3))->i, 8);

  g_queue_push_tail(test0, allocator->create_int(-1));
  native_index(test0);
  ASSERT_EQ(((Object*)g_queue_peek_tail(test0))->type, Exception);

  g_queue_free(objs);
  g_queue_free(test0);
  g_queue_free(test1);
  g_queue_free(test2);
  g_queue_free(test3);
}

TEST(OpRoll, Typical) {
  Stack objs = g_queue_new();

  g_queue_push_tail(objs, a_->create_string("a"));
  g_queue_push_tail(objs, a_->create_string("b"));
  g_queue_push_tail(objs, a_->create_string("c"));

  Stack bca = g_queue_copy(objs);
  g_queue_push_tail(bca, allocator->create_int(3));
  g_queue_push_tail(bca, allocator->create_int(-1));

  Stack cab = g_queue_copy(objs);
  g_queue_push_tail(cab, allocator->create_int(3));
  g_queue_push_tail(cab, allocator->create_int(1));

  Stack abc = g_queue_copy(objs);
  g_queue_push_tail(abc, allocator->create_int(3));
  g_queue_push_tail(abc, allocator->create_int((int64_t)0));

  native_roll(bca);
  native_roll(cab);
  native_roll(abc);

  ASSERT_EQ(*((Object*)g_queue_peek_nth(bca,0))->str->str, "b");
  ASSERT_EQ(*((Object*)g_queue_peek_nth(bca,1))->str->str, "c");
  ASSERT_EQ(*((Object*)g_queue_peek_nth(bca,2))->str->str, "a");

  ASSERT_EQ(*((Object*)g_queue_peek_nth(cab,0))->str->str, "c");
  ASSERT_EQ(*((Object*)g_queue_peek_nth(cab,1))->str->str, "a");
  ASSERT_EQ(*((Object*)g_queue_peek_nth(cab,2))->str->str, "b");

  ASSERT_EQ(*((Object*)g_queue_peek_nth(abc,0))->str->str, "a");
  ASSERT_EQ(*((Object*)g_queue_peek_nth(abc,1))->str->str, "b");
  ASSERT_EQ(*((Object*)g_queue_peek_nth(abc,2))->str->str, "c");

  g_queue_free(cab);
  g_queue_free(bca);
  g_queue_free(abc);
  g_queue_free(objs);
}

