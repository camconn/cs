/*
 * font.hpp - Font Utilities
 *
 * © 2020 Cameron Conn
 */

#ifndef CS_FONT_H
#define CS_FONT_H

#include <cstdint>

#include <map>
#include <string>

#include <cairo.h>

bool setup_fonts();
void cleanup_fonts();

bool use_font(int64_t);
int64_t find_font(const char*);
int64_t make_scaled_font(int64_t, double);
int64_t scale_font(int64_t, double);

void show_text(const std::string&);

#endif
