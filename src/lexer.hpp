/*
 * lexer.hpp - Interface to flex for CS
 *
 * © 2020 Cameron Conn
 */

#ifndef CS_LEXER_H
#define CS_LEXER_H

#include <string>

#include "lex.yy.h"

#include "interpreter.hpp"


int lexer(Stack, FILE*);

std::string translate_escaped_string(std::string&);
std::string translate_hex_string(std::string&);


#endif
