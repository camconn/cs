/*
 * interpreter.hpp - PostScript interpreter.
 *
 * © 2020 Cameron Conn
 */

#ifndef CS_INTERPRETER_H
#define CS_INTERPRETER_H

#include <glib.h>

#include "memory.hpp"
#include "object.hpp"

int execute(GQueue*, GQueue*);

#endif

