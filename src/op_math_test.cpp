/*
 * op_math_test.cpp - Test math functions
 *
 * © 2020 Cameron Conn
 */

#include <cmath>
#include <cstdint>

#include <glib.h>

#include "gtest/gtest.h"

#include "interpreter.hpp"
#include "object.hpp"
#include "object_test.hpp"
#include "op.hpp"

Stack fake_stack_ii(int64_t a, int64_t b) {
  Stack stk = g_queue_new();
  g_queue_push_tail(stk, allocator->create_int(a));
  g_queue_push_tail(stk, allocator->create_int(b));
  return stk;
}
Stack fake_stack_dd(double a, double b) {
  Stack stk = g_queue_new();
  g_queue_push_tail(stk, allocator->create_real(a));
  g_queue_push_tail(stk, allocator->create_real(b));
  return stk;
}
Stack fake_stack_id(int64_t a, double b) {
  Stack stk = g_queue_new();
  g_queue_push_tail(stk, allocator->create_int(a));
  g_queue_push_tail(stk, allocator->create_real(b));
  return stk;
}
Stack fake_stack_di(double a, int64_t b) {
  Stack stk = g_queue_new();
  g_queue_push_tail(stk, allocator->create_real(a));
  g_queue_push_tail(stk, allocator->create_int(b));
  return stk;
}


TEST(Math, Exp) {
  init_vm();
  Stack a = fake_stack_ii(2, 3);
  Stack b = fake_stack_id(5, 3.8);

  native_exp(a);
  native_exp(b);

  auto eight = allocator->create_int(pow(2,3));
  auto fourfivetwo = allocator->create_real(pow(5,3.8));

  ASSERT_EQ(*((Object*)g_queue_peek_tail(a)), *eight);
  ASSERT_EQ(*((Object*)g_queue_peek_tail(b)), *fourfivetwo);

  g_queue_free(a);
  g_queue_free(b);
}

TEST(Math, Add) {
  init_vm();
  Stack a = fake_stack_ii(2, 3);
  Stack b = fake_stack_id(5, 3.8);

  native_add(a);
  native_add(b);

  ASSERT_EQ(*((Object*)g_queue_peek_tail(a)),
            *allocator->create_int(5));
  ASSERT_EQ(*((Object*)g_queue_peek_tail(a)),
            *allocator->create_int(2+3));
  ASSERT_EQ(*((Object*)g_queue_peek_tail(b)),
            *allocator->create_real(5.0 + 3.8));

  g_queue_free(a);
  g_queue_free(b);
}


TEST(Math, Sub) {
  init_vm();
  Stack a = fake_stack_ii(2, 3);
  Stack b = fake_stack_id(5, 3.8);

  native_sub(a);
  native_sub(b);

  auto c = allocator->create_real(5);
  auto d = allocator->create_real(3.8);
  //auto e = c - d;

  ASSERT_EQ(*((Object*)g_queue_peek_tail(a)), *allocator->create_int(-1));
  //ASSERT_EQ(b->back(), e);
}

TEST(Math, Mul) {
  init_vm();
  Stack a = fake_stack_di(2.5, 2);
  Stack b = fake_stack_id(5, 3.8);
  Stack c = fake_stack_ii(5, 1);

  native_mul(a);
  native_mul(b);
  native_mul(c);

  auto five = allocator->create_int(5);

  ASSERT_EQ(*((Object*)g_queue_peek_tail(a)), *five);
  ASSERT_EQ(*((Object*)g_queue_peek_tail(a)),
            *allocator->create_real(2.5 * 2));
  ASSERT_EQ(*((Object*)g_queue_peek_tail(b)),
            *allocator->create_real(5.0 * 3.8));
  ASSERT_EQ(*((Object*)g_queue_peek_tail(c)), *five);

}

TEST(Math, Div) {
  init_vm();
  Stack a = fake_stack_ii(2, 3);
  Stack b = fake_stack_id(5, 3.8);
  Stack c = fake_stack_ii(10, 0);
  Stack d = fake_stack_id(10, -2.0);

  native_div(a);
  native_div(b);
  native_div(c);
  native_div(d);

  auto exception = allocator->create_exception();
  auto five = allocator->create_real(-5);
  auto e = allocator->create_real(2.0/3.0);
  auto f = allocator->create_real(5.0 / 3.8);

  ASSERT_EQ(*((Object*)g_queue_peek_tail(a)), *e);
  ASSERT_EQ(*((Object*)g_queue_peek_tail(b)), *f);
  ASSERT_EQ(*((Object*)g_queue_peek_tail(c)), *exception);
  ASSERT_EQ(*((Object*)g_queue_peek_tail(d)), *five);

  g_queue_free(a);
  g_queue_free(b);
  g_queue_free(c);
  g_queue_free(d);
}
