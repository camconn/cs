/*
 * main_test.cpp - Test Runner
 *
 * © 2020 Cameron Conn
 */

#include <glib.h>

#include "gtest/gtest.h"

#include "memory.hpp"


int main(int argc, char **argv) {
  testing::InitGoogleTest(&argc, argv);
  return RUN_ALL_TESTS();
}
