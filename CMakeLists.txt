cmake_minimum_required (VERSION 3.10)

project(cs VERSION 0.1.0)

set(CXX_STANDARD 11)
set(CMAKE_CXX_STANDARD 11)

find_package(PkgConfig REQUIRED)
find_package(FLEX REQUIRED)
find_package(Freetype REQUIRED)
find_package(Fontconfig REQUIRED)

# pkg-config dependencies
pkg_check_modules(CAIRO cairo REQUIRED)
pkg_check_modules(GTK3 gtk+-3.0 REQUIRED)

if(NOT CAIRO_FOUND)
  message(error, "Cairo is required")
endif(NOT CAIRO_FOUND)

if(NOT GTK3_FOUND)
  message(error, "GTK3 is required")
endif(NOT GTK3_FOUND)

# Used for profiling compile times (which are ridiculous with C++ STL)
#set(CMAKE_CXX_FLAGS "${CMAKE_CXX_FLAGS} -ftime-report -Q")


#### Google Tests
# Download and unpack googletest at configure time
configure_file(CMakeLists.txt.in googletest-download/CMakeLists.txt)
execute_process(COMMAND ${CMAKE_COMMAND} -G "${CMAKE_GENERATOR}" .
  RESULT_VARIABLE result
  WORKING_DIRECTORY ${CMAKE_CURRENT_BINARY_DIR}/googletest-download )
if(result)
  message(FATAL_ERROR "CMake step for googletest failed: ${result}")
endif()
execute_process(COMMAND ${CMAKE_COMMAND} --build .
  RESULT_VARIABLE result
  WORKING_DIRECTORY ${CMAKE_CURRENT_BINARY_DIR}/googletest-download )
if(result)
  message(FATAL_ERROR "Build step for googletest failed: ${result}")
endif()

# Prevent overriding the parent project's compiler/linker
# settings on Windows
set(gtest_force_shared_crt ON CACHE BOOL "" FORCE)

# Add googletest directly to our build. This defines
# the gtest and gtest_main targets.
add_subdirectory(${CMAKE_CURRENT_BINARY_DIR}/googletest-src
                 ${CMAKE_CURRENT_BINARY_DIR}/googletest-build
                 EXCLUDE_FROM_ALL)

include(GoogleTest)
#### End Google Tests


# Actual CS source
add_subdirectory("src")

