# CS
Crummy PostScript Interpreter

# About
A baby PostScript interpreter built with the Cairo Graphics Library
and GTK3.

This PostScript implementation supports the concatenation of strings!
Try it out with `(Hello, ) (World!) add`.

# Requirements
- cmake
- Cairo
- GTK3+
- flex
- g++/clang++

# Building
In the root directory:

```
cmake -B build -DCMAKE_EXPORT_COMPILE_COMMANDS=YES -DCMAKE_BUILD_TYPE=Debug
cd build
make
```

# Tests
This project uses google tests. To make and run tests do the following:
```
# Run the cmake command in the `building` section
cd build
make cstest

# run the tests
make test
# or for more detailed output, run the suite manually:
src/cstest
```

# Licensing
Licensed under the GNU Affero General Public License, version 3 or any
later version at your choosing. A copy of this license is located in
`LICENSE.txt`.

If you need a different license for your uses, please contact
`cam remove@this camconn dot cc` to negotiate terms for a different
license.
